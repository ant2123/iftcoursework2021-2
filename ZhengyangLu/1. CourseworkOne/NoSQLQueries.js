use Equity

db



1. we have had stock "A" in our portfolio, so we need to decide other stocks to headge the risk of stock "A" in order to reduce the overall risk of portfolio.

--First to check "A"s data with more details:
 
db.Coursework1.find({"Symbol":"A"},{"StaticData.GICSSector":1, "MarketData":1, "FinancialRatios":1});

--We have got all data about "A".

2.Checking the industry wide avergae Dividend Yield:

var pipeline1 =([
	{$match: {"StaticData.GICSSector": "Real Estate"} },
	{$group: {_id: "$StaticData.GICSSector", SectorAverageDividendYield: {$avg:"$FinancialRatios.DividendYield"} }}
    
    ]);


db.Coursework1.aggregate(pipeline1)

3. We create a new pipeline to search stcoks that we want to use for hedging.

var pipeline2 =([ 
    {$project: {_id:0}},
    {$match:{$and:[{"StaticData.GICSSector":"Real Estate"},{"MarketData.Beta":{$lte:0.9}}]}},
    {$sort: {"FinancialRatios.PERatio":1} },
    {$limit: 6}
    
    ]);


db.Coursework1.aggregate(pipeline2)

--since we have our targets, we can decide which stock we will be using for hedging.