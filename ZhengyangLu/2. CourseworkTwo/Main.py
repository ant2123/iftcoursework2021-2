
### Directory

GITRepoDirectory = "D:/UCL/T1/Big data/Coursework 2/"

import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import datetime
from pymongo import MongoClient
import configparser

### connecting to SQLite

config_obj = configparser.ConfigParser()
config_obj.read(f"{GITRepoDirectory}/iftcoursework2021/ZhengyangLu/2. CourseworkTwo/config/script.config")


year = config_obj.get('database1','courseyear')
data = config_obj.get('database1','data')
type = config_obj.get('database1','type')
database = config_obj.get('database1','database')

engine = create_engine(f"sqlite:///{GITRepoDirectory}/" +year+ data +type +database)
sqlcon = engine.connect()

### Selecting data from SQL

def SQLtechPrice():
    return ("SELECT equity_static.symbol, equity_static.GICSSector,equity_prices.cob_date, equity_prices.close FROM equity_static join equity_prices on equity_static.symbol= equity_prices.symbol_id and equity_static.GICSSector = 'Information Technology' and equity_prices.cob_date like '%2021%'")

df = pd.read_sql_query(SQLtechPrice(),engine)
TechPrice = pd.DataFrame(df, columns = ['symbol','GICSSector','cob_date','close'])
TechPrice = TechPrice[TechPrice.symbol != 'FLIR']
TechPrice = TechPrice[TechPrice.symbol != 'MXIM']

### Modifying dataframe

date_strings = TechPrice["cob_date"].to_list()
date = []

for i in range(len(date_strings)):
    date += [datetime.datetime.strptime(date_strings[i], "%d-%b-%Y")]
TechPrice['datetime']= date
TechPrice['close'] = TechPrice['close'].apply(pd.to_numeric,errors='coerce')
TechPrice.rename(columns = {'symbol':'Symbol','close':'Close','datetime':'Date'},inplace = True)

TechPrice = TechPrice.drop(['cob_date','GICSSector'],1)

TechPrice = TechPrice.groupby(['Date', 'Symbol'])['Close'].first().unstack()
print(TechPrice)

### Connecting to mongoDB

config_obj.read(f"{GITRepoDirectory}/iftcoursework2021/ZhengyangLu/2. CourseworkTwo/config/script.params")
host = config_obj.get('mongo','host')

mongocon = MongoClient(host)
db = mongocon.Equity
col = db.Coursework1

### Selecting data from MongoDB

pipeline = [
    {'$match':{'StaticData.GICSSector':'Information Technology'}},
    {"$unwind":{'path': '$MarketData', 'preserveNullAndEmptyArrays': True}},
    {"$unwind":{'path': '$MarketData.MarketCap', 'preserveNullAndEmptyArrays': True}},
    {'$project':{
      'Symbol':'$Symbol',
      'MarketCap':"$MarketData.MarketCap" 
    }}
]

df1 = pd.DataFrame()
for doc in col.aggregate(pipeline): 
    df1 = df1.append(doc,ignore_index=True)

### Modyfying dataframe

TechCap = pd.DataFrame(df1, columns = ['Symbol','MarketCap'])
TechCap['MarketCap'] = TechCap['MarketCap'].apply(pd.to_numeric,errors='coerce')

TechCap = TechCap.sort_values('Symbol',ascending = True)

TechCap = TechCap[TechCap.Symbol != 'LRCX']
TechCap = TechCap[TechCap.Symbol != 'RHT']
TechCap = TechCap[TechCap.Symbol != 'TSS']
TechCap = TechCap[TechCap.Symbol != 'XRX']
TechCap = TechCap[TechCap.Symbol != 'FLIR']
TechCap = TechCap[TechCap.Symbol != 'MXIM']

### Calculating MarketCap Weights

SumCap = TechCap['MarketCap'].sum()
TechCap['Weight'] = 0
TechCap['Weight'] += TechCap['MarketCap']/SumCap
print(TechCap)

### Calculating Benchmark prices

TechPrice_mat = np.array(TechPrice)
weight_vec = np.array(TechCap['Weight'])
index = TechPrice_mat@weight_vec
print(index)

TechPrice['Benchmark'] = 0
TechPrice['Benchmark'] += index

### Calculating benchmark daily returns

TechPrice['BenchmarkDailyReturn'] = TechPrice['Benchmark'].pct_change(1)
TechPrice['BenchmarkDailyReturn'] = TechPrice['BenchmarkDailyReturn'].fillna(0)
print(TechPrice)

### Creating new table to SQL

benchmarkdf = TechPrice[['Benchmark','BenchmarkDailyReturn']].copy()
benchmarkdf

benchmarkdf.to_sql('benchmark_returns', sqlcon, if_exists='replace')

### Selecting portfolio info from SQL

def SQLDGRpos():
    return ("SELECT portfolio_positions.cob_date, portfolio_positions.symbol,portfolio_positions.net_quantity,portfolio_positions.net_amount, equity_prices.close from portfolio_positions inner join equity_prices on portfolio_positions.symbol = equity_prices.symbol_id And portfolio_positions.cob_date = equity_prices.cob_date where portfolio_positions.trader = 'DGR1983' AND portfolio_positions.cob_date = '10-Nov-2021'")

### Modifying dataframe for portfolio


df2 = pd.read_sql_query(SQLDGRpos(),engine)
Portfolio = pd.DataFrame(df2, columns = ['cob_date','symbol','net_quantity','net_amount','close'])

date_strings2 = Portfolio["cob_date"].to_list()
date2 = []

for i in range(len(date_strings2)):
    date2 += [datetime.datetime.strptime(date_strings2[i], "%d-%b-%Y")]
Portfolio['datetime']= date2
Portfolio['close'] = Portfolio['close'].apply(pd.to_numeric,errors='coerce')
Portfolio['net_quantity'] = Portfolio['net_quantity'].apply(pd.to_numeric,errors='coerce')
Portfolio['net_amount'] = Portfolio['net_amount'].apply(pd.to_numeric,errors='coerce')
Portfolio.rename(columns = {'symbol':'Symbol','close':'Close','datetime':'Date','net_quantity':'Positions','net_amount':'NotionalValue'},inplace = True)
Portfolio = Portfolio.drop(['cob_date'],1)

Portfolio['MarketValue'] = 0
Portfolio['MarketValue'] += Portfolio['Positions'] * Portfolio['Close']

### Calculating Weights for portfolio

TotalNotionalValue = np.sum(Portfolio['NotionalValue'])
MarketValue = np.sum(Portfolio['MarketValue'])

print(TotalNotionalValue)
print(MarketValue)

Portfolio['PWeights'] = 0
Portfolio['PWeights'] += Portfolio['MarketValue']/MarketValue
Portfolio['NWeights'] = 0
Portfolio['NWeights'] += Portfolio['NotionalValue']/TotalNotionalValue

PortfolioWeights = Portfolio.copy().drop(['Positions','NotionalValue','Close','Date','MarketValue'],1)

### Finalising portfolio prices info

symbol = PortfolioWeights['Symbol'].tolist()
df3 = TechPrice.copy()
PortPrice = df3[symbol]

### Calculating portfolio returns

PortPriceDated = PortPrice.iloc[-23:]
PortPriceDated.drop(PortPriceDated.tail(2).index, inplace = True)

PortReturn = PortPriceDated.pct_change(1)
PortReturn = PortReturn.fillna(0)
PortReturn.drop(index=PortReturn.index[0], axis=0, inplace=True)

PortReturn_mat = np.array(PortReturn)
Pweight_vec = np.array(PortfolioWeights['PWeights'])
Nweight_vec = np.array(PortfolioWeights['NWeights'])

preturn = PortReturn_mat@Pweight_vec
nreturn = PortReturn_mat@Nweight_vec


PortReturn['PWeightsReturn'] = 0
PortReturn['NWeightsReturn'] = 0
PortReturn['PWeightsReturn'] += preturn
PortReturn['NWeightsReturn'] += nreturn

WeightReturn = PortReturn.drop(symbol,1)

### Adding benchmark returns for comparison

Benchmark = benchmarkdf.iloc[-22:]
Benchmark.drop(Benchmark.tail(2).index, inplace = True)

bench = np.array(Benchmark['BenchmarkDailyReturn'])

WeightReturn['Benchmark'] = 0
WeightReturn['Benchmark'] += bench

WeightReturn['PReturn_Higher'] = WeightReturn['PWeightsReturn'] > WeightReturn['Benchmark']
WeightReturn['NReturn_Higher'] = WeightReturn['NWeightsReturn'] > WeightReturn['Benchmark']
print(WeightReturn)

### Checking the results

print('Number of Days that the portfolio has outperformed the benchmark by using Market Value weights:',sum(WeightReturn.PReturn_Higher== True))
print('\nNumber of Days that the portfolio has outperformed the benchmark by using Notional Value weights:',sum(WeightReturn.NReturn_Higher== True))

### Closing connection

sqlcon.close()
mongocon.close()



























