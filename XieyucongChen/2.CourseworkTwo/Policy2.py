from scipy.stats import norm
import pandas as pd
import numpy as np

# Policy 2: volume relative (%): max position given daily volumes. The trader cannot have more than X% of daily volume as ratio of position consideration. position must be mark to market over the volume;

def compute_average_daily_volume(df):
    history_volume = df.groupby(['Trader', 'Currency'])['SumNotional'].sum()
    day_counts = df.groupby(['Trader', 'Currency'])['Date'].nunique()
    return history_volume/day_counts

def sum_stock(df):
    return df.groupby(['Trader', 'Currency', 'Date'])['SumNotional'].sum()

def check_policy2(df, limit_dict):
    average_daily_volume = compute_average_daily_volume(df)
    df2 = sum_stock(df)

    breaches = []
    for id, agg in df2.items():
    
        # consider only 2021-11-11 and 2021-11-12
        if id[2] != "2021-11-11" and id[2] != "2021-11-12":
            continue
        volume_relative_limit = limit_dict[(id[0], "volume", "relative", "PCG")]
        average = average_daily_volume[(id[0], id[1])]

        volume_relative = (agg - average) / average * 100
        if average > 0 and volume_relative > volume_relative_limit[0]:
            breaches.append((2, volume_relative_limit[1], volume_relative, id[2]))
            # print("### Volume relative breach")
            # print(id, agg)
            # print("Average:", average)
            # print("Limit:", volume_relative_limit)
            # print()
    return breaches