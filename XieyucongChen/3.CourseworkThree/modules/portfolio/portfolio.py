import pandas as pd
import numpy as np
import json

from modules.write.write_to_file import *

RISK_FREE_RATE = 0.05
EXPECT_RETURN_OF_MARKET = 0.105

def portfolios_for_stock_to_string(portfolios_for_stock):
    portfolios_for_stock_dict = {}
    for stock in portfolios_for_stock:
        portfolios_for_stock_dict[stock] = {}
        portfolios_for_stock_dict[stock]["capital_gain"] = portfolios_for_stock[stock][0]
        portfolios_for_stock_dict[stock]["close_mean"] = portfolios_for_stock[stock][1]
        portfolios_for_stock_dict[stock]["volatility"] = portfolios_for_stock[stock][2]
        portfolios_for_stock_dict[stock]["return"] = portfolios_for_stock[stock][3]
        portfolios_for_stock_dict[stock]["changes"] = portfolios_for_stock[stock][4].values.tolist()

    s = json.dumps(portfolios_for_stock_dict)
    return s

def portfolios_over_stocks_to_string(portfolio_positions, portfolio_return, portfolio_net_quantity, portfolio_net_amount, portfolio_volatility, sharpe, long_exposure, short_exposure, net_exposure, beta, alpha, expect_return, excess_return, sortino_ratio, treynor_ratio, information_ratio):
    portfolios_over_stocks_dict = {}
    portfolios_over_stocks_dict["portfolio_return"] = portfolio_return
    portfolios_over_stocks_dict["portfolio_net_quantity"] = float(portfolio_net_quantity)
    portfolios_over_stocks_dict["portfolio_net_amount"] = float(portfolio_net_amount)
    portfolios_over_stocks_dict["portfolio_volatility"] = portfolio_volatility
    portfolios_over_stocks_dict["sharpe"] = sharpe
    portfolios_over_stocks_dict["long_exposure"] = long_exposure
    portfolios_over_stocks_dict["short_exposure"] = short_exposure
    portfolios_over_stocks_dict["net_exposure"] = net_exposure
    portfolios_over_stocks_dict["beta"] = beta
    portfolios_over_stocks_dict["alpha"] = alpha
    portfolios_over_stocks_dict["expect_return"] = expect_return
    portfolios_over_stocks_dict["excess_return"] = excess_return
    portfolios_over_stocks_dict["sortino_ratio"] = sortino_ratio
    portfolios_over_stocks_dict["treynor_ratio"] = treynor_ratio
    portfolios_over_stocks_dict["information_ratio"] = information_ratio

    s = json.dumps(portfolios_over_stocks_dict)
    return s

def matrix_to_string(matrix):
    return matrix.to_json()

# output the values to files
def compute_portfolios(df, trade_type_df, trader, con):
    
    print("Computing the portfolios for each stock...")
    portfolios_for_stock = {}
    for index, agg in df.iterrows():
        if agg["Date"] != "2021-11-12" or agg["Trader"] != trader:
            continue

        stock = agg["Symbol"]

        capital_gain, close_mean, volatility, profit, changes, beta = portfolios_per_stock(trader, stock, con)
        
        portfolios_for_stock[stock] = (capital_gain, close_mean, volatility, profit, changes, beta)
    
    print("Computing the portfolios over all stocks...")
    portfolio_positions, portfolio_return, portfolio_net_quantity, portfolio_net_amount, portfolio_volatility, sharpe, long_exposure, short_exposure, net_exposure, beta, alpha, expect_return, excess_return, sortino_ratio, treynor_ratio, information_ratio = portfolios_over_stocks(trader, portfolios_for_stock, trade_type_df, con)
    
    
    print("Computing portfolios over stock pairs...")
    corr_matrix = portfolios_corr_stocks(con)
    portfolio_sd_matrix = portfolio_sd(portfolios_for_stock, portfolio_positions, corr_matrix, con)


    ## output to files
    print("Writing portfolios to files...")
    write_str("each_stock.json", portfolios_for_stock_to_string(portfolios_for_stock))
    write_str("over_stocks.json", portfolios_over_stocks_to_string(portfolio_positions, portfolio_return, portfolio_net_quantity, portfolio_net_amount, portfolio_volatility, sharpe, long_exposure, short_exposure, net_exposure, beta, alpha, expect_return, excess_return, sortino_ratio, treynor_ratio, information_ratio))

    write_str("corr_matrix.json", matrix_to_string(corr_matrix))
    write_str("sd_matrix.json", matrix_to_string(portfolio_sd_matrix))

    print("Done")



def portfolios_per_stock(trader, stock, con):
    # get the df for the stock
    equity_prices = pd.read_sql_query(f"SELECT * FROM equity_prices WHERE symbol_id = '{stock}'", con)
    portfolio_positions = pd.read_sql_query(f"SELECT trader, symbol, ccy, SUM(net_quantity) AS sum_net_quantity, SUM(net_amount) AS sum_net_amount FROM portfolio_positions WHERE symbol = '{stock}' AND trader = '{trader}' GROUP BY trader, symbol, ccy", con)

    capital_gain = compute_capital_gain(equity_prices)
    close_mean = compute_close_mean(equity_prices)
    volatility = compute_volatility(equity_prices, "20210923", "20211112", stock)
    profit = compute_profit(equity_prices, portfolio_positions)
    changes = compute_changes(equity_prices)

    # Beta
    SP500 = pd.read_csv('./static/SP500.csv')
    SP500_daily_change_percent = SP500["% change"].mean() * 0.01
    stock_daily_change_percent = capital_gain
    beta = (stock_daily_change_percent * SP500_daily_change_percent) / SP500_daily_change_percent


    return capital_gain, close_mean, volatility, profit, changes, beta

def portfolios_over_stocks(trader, portfolios_for_stock, trade_type_df, con):
    portfolio_positions = pd.read_sql_query(f"SELECT trader, symbol, ccy, SUM(net_quantity) AS sum_net_quantity, SUM(net_amount) AS sum_net_amount FROM portfolio_positions WHERE trader = '{trader}' GROUP BY trader, symbol, ccy", con)

    # (capital_gain, close_mean, volatility, profit)

    # compute stock weights
    total_net_amount = portfolio_positions['sum_net_amount'].sum()
    portfolio_positions['weight'] = portfolio_positions['sum_net_amount'] / total_net_amount


    portfolio_return = compute_portfolio_return(portfolio_positions, portfolios_for_stock)
    portfolio_volatility = compute_portfolio_volatility(portfolio_positions, portfolios_for_stock)
    

    sharpe = (portfolio_return - RISK_FREE_RATE) / portfolio_volatility
    
    portfolio_net_quantity = portfolio_positions["sum_net_quantity"].sum()
    portfolio_net_amount = portfolio_positions["sum_net_amount"].sum()

    long_exposure, short_exposure = compute_long_short_exposure(trade_type_df, trader)
    net_exposure = long_exposure - short_exposure

    # beta, alpha, ...
    beta = compute_portfolio_beta(portfolio_positions, portfolios_for_stock)

    alpha = portfolio_return - (RISK_FREE_RATE + beta * (EXPECT_RETURN_OF_MARKET - RISK_FREE_RATE))

    expect_return = RISK_FREE_RATE + beta * (EXPECT_RETURN_OF_MARKET - RISK_FREE_RATE)

    excess_return = RISK_FREE_RATE + beta * (EXPECT_RETURN_OF_MARKET - RISK_FREE_RATE) - portfolio_return

    sortino_ratio = (excess_return - RISK_FREE_RATE) / portfolio_volatility

    treynor_ratio = (portfolio_return - RISK_FREE_RATE) / beta

    information_ratio = (portfolio_return - EXPECT_RETURN_OF_MARKET) / portfolio_volatility


    return portfolio_positions, portfolio_return, portfolio_net_quantity, portfolio_net_amount, portfolio_volatility, sharpe, long_exposure, short_exposure, net_exposure, beta, alpha, expect_return, excess_return, sortino_ratio, treynor_ratio, information_ratio

def portfolios_corr_stocks(con):
    equity_prices = pd.read_sql_query(f"SELECT * FROM equity_prices", con)

    stocks = equity_prices["symbol_id"].unique()
    stock_closes = {}

    for stock in stocks:
        closes = equity_prices[equity_prices["symbol_id"] == stock]["close"].values
        if len(closes) != 472:
            continue

        # print(len(closes), stock)
        stock_closes[stock] = closes
    
    stock_closes_df = pd.DataFrame(stock_closes)
    corr_matrix = stock_closes_df.corr()

    return corr_matrix

def portfolio_sd(portfolios_for_stock, portfolio_positions, corr_matrix, con):

    df_portfolio_sd = pd.DataFrame() # Correlation matrix

    for index1, row1 in portfolio_positions.iterrows():
        
        for index2, row2 in portfolio_positions.iterrows():
            stock1 = row1['symbol']
            stock2 = row2['symbol']

            if not stock1 in portfolios_for_stock or not stock2 in portfolios_for_stock:
                continue
            sd_1 = portfolios_for_stock[stock1][2]
            sd_2 = portfolios_for_stock[stock2][2]
            weight_1 = row1['weight']
            weight_2 = row2['weight']

            corr = corr_matrix[stock1][stock2]

            portfolio_sd = (sd_1 ** 2) * (weight_1 ** 2) + 2 * weight_1 * weight_2 * (corr ** 2) + (sd_2 ** 2) * (weight_2 ** 2)

            df_portfolio_sd.loc[stock1, stock2] = portfolio_sd

    return df_portfolio_sd

def compute_portfolio_beta(portfolio_positions, portfolios_for_stock):
    weighted_beta = 0
    for index, row in portfolio_positions.iterrows():
        stock = row['symbol']

        if not stock in portfolios_for_stock:
            continue
        beta = portfolios_for_stock[stock][5]
        weight = row['weight']

        weighted_beta += weight * beta

    return weighted_beta


def compute_portfolio_return(portfolio_positions, portfolios_for_stock):
    weighted_return = 0
    for index, row in portfolio_positions.iterrows():
        stock = row['symbol']

        if not stock in portfolios_for_stock:
            continue
        stock_profit = portfolios_for_stock[stock][3]
        weight = row['weight']

        weighted_return += weight * stock_profit

    return weighted_return

def compute_portfolio_volatility(portfolio_positions, portfolios_for_stock):
    weighted_volatility = 0
    for index, row in portfolio_positions.iterrows():
        stock = row['symbol']

        if not stock in portfolios_for_stock:
            continue
        volatility = portfolios_for_stock[stock][2]
        weight = row['weight']

        weighted_volatility += weight * volatility

    return weighted_volatility

def compute_long_short_exposure(trade_type_df, trader):
    trade_type_df = trade_type_df[trade_type_df['Trader'] == trader]
    trade_type_agg = trade_type_df.groupby(['Trader', 'Currency', 'TradeType'])['SumNotional'].sum()

    buy_amount = np.abs(trade_type_agg[(trader, 'USD', 'BUY')])
    sell_amount = np.abs(trade_type_agg[(trader, 'USD', 'SELL')])

    long_exposure = buy_amount / (buy_amount + sell_amount)
    short_exposure = sell_amount / (buy_amount + sell_amount)

    return long_exposure, short_exposure

def compute_capital_gain(equity_prices):
    stockData = equity_prices["close"]
    return stockData.pct_change().dropna().mean()

def compute_changes(equity_prices):
    stockData = equity_prices["close"]
    return stockData.pct_change().dropna()

def compute_close_mean(equity_prices):
    return equity_prices["close"].mean()

def compute_volatility(equity_prices, start_date, end_date, stock):
    # apply dates
    equity_prices = equity_prices[equity_prices["price_id"] >= f"{start_date}{stock}"]
    equity_prices = equity_prices[equity_prices["price_id"] <= f"{end_date}{stock}"]

    volatility = np.std(equity_prices["close"] - equity_prices["open"])

    return volatility

# 盈亏 return
def compute_profit(equity_prices, portfolio_positions):
    close_price = equity_prices[equity_prices['cob_date'] == "12-Nov-2021"]['close'][0]
    row = portfolio_positions.values.tolist()[0]

    sum_net_amount = row[4]
    sum_net_quantity = row[3]
    if len(portfolio_positions) > 0:
        profit = sum_net_amount - close_price * sum_net_quantity
        return profit
    else:
        return 0

