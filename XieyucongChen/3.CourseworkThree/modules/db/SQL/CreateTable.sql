DROP TABLE IF EXISTS "policy_breaches";

CREATE TABLE "policy_breaches" (
	"policy_breach_id"	INTEGER NOT NULL, -- the unique id
	"policy_type"	INTEGER NOT NULL, -- the type of the policy, i.e., policy 1 is for long/short consideration policy
	"trader_limit_id"	TEXT NOT NULL, -- the trader limit id in the trader_limit table
	"symbol"	TEXT NOT NULL, -- the symbol of the breach
    "breach_value"  NUMERIC NOT NULL, -- the value that breaches the limit
    "breach_date" TEXT NOT NULL, -- the date that the breach happened
	"detect_time"	TEXT NOT NULL, -- the datetime that the breach is found
	PRIMARY KEY("policy_breach_id" AUTOINCREMENT)
);

