
def recreate_table(con):
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS "portfolio_positions";')
    sql = """CREATE TABLE portfolio_positions (
    pos_id TEXT PRIMARY KEY, -- unique identifier made of traderid + cob_date (Ymd) + symbol_id
    cob_date TEXT NOT NULL,  -- date in format of DD-Mmm-YYYY
    trader TEXT NOT NULL,    -- trader identifier
    symbol TEXT NOT NULL,    -- stock identifier
    ccy TEXT NOT NULL,       -- currency for notional amount   
    net_quantity INTEGER NOT NULL,      -- aggregation of all trades made in a given date by trader & stock as sum
    net_amount INTEGER NOT NULL,        -- aggregation of all notional made in a given date by trader & stock as sum
    FOREIGN KEY (symbol) REFERENCES equity_static (symbol)
);
"""
    cur.execute(sql)

def generate_insert_tuple(row):
    tmp_date = row["Date"].split("-")
    cob_date = f"{tmp_date[2]}-{tmp_date[1]}-{tmp_date[0]}"
    trader = row["Trader"]
    symbol = row["Symbol"]
    ccy = row["Currency"]
    net_quantity = row["SumQuantity"]
    net_amount = row["SumNotional"]

    pos_id = f"{trader}{''.join(tmp_date)}{symbol}"

    return (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)


def save_portfolio_positions(df, con, recreate=False):
    if recreate:
        recreate_table(con)

    portfolio_positions = []
    for index, row in df.iterrows():
        portfolio_positions.append(generate_insert_tuple(row))

    sql = "INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount) VALUES (?,?,?,?,?,?,?);"

    cur = con.cursor()
    cur.executemany(sql, portfolio_positions)

    print('Inserted', cur.rowcount, 'records to the table.')

    con.commit()
