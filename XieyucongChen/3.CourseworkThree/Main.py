from modules.db.db_connection import *
from modules.write.save_breaches import save_breaches
from modules.write.save_portfolio_positions import save_portfolio_positions
from modules.read.agg_trade_data import read_agg_trade_data, read_trade_type_trade_data
from modules.read.trader_limits import read_trader_limits

from modules.policies.Policy1 import *
from modules.policies.Policy2 import *
from modules.policies.Policy3 import *
from modules.policies.Policy4 import *
from modules.policies.Policy5 import *
from modules.policies.Policy6 import *

from modules.portfolio.portfolio import *

from configparser import ConfigParser
import argparse


####### Parse command line parameters
params = ConfigParser()
params.read('./config/script.params')

# default parameters values
recreate_portfolio_positions = params["DEFAULT"]['recreate_portfolio_positions']
recreate_policy_breaches = params["DEFAULT"]['recreate_policy_breaches']

# read from command line inputs
parser = argparse.ArgumentParser(description='Detect breaches. Change the default values in config/script.params')

parser.add_argument('-p','--positions', action='store_true', help=f'Drop and recreate the portfolio_positions table. Default to be {recreate_portfolio_positions}', required=False, default=recreate_portfolio_positions)
parser.add_argument('-b','--breaches', action='store_true', help=f'Drop and recreate the policy_breaches table. Default to be {recreate_policy_breaches}', required=False, default=recreate_policy_breaches)
args = vars(parser.parse_args())

recreate_portfolio_positions = args["positions"]
recreate_policy_breaches = args["breaches"]

print(f"Drop and recreate the portfolio_positions table = {recreate_portfolio_positions}")
print(f"Drop and recreate the policy_breaches table = {recreate_policy_breaches}")
print()

####### Read config 
config = ConfigParser()
config.read('./config/script.config')

####### Connect to databases

client, CourseworkTwo = connect_mongodb(config["DATABASE"]["mongodb_address"], config["DATABASE"]["mongodb_port"]) # connect to mongo db

con = connect_sqlite(config["DATABASE"]["sqlite_db_file"]) # connect to sqlite 

####### Read trade data from mongodb

df = read_agg_trade_data(CourseworkTwo)
trade_type_df = read_trade_type_trade_data(CourseworkTwo)

####### Read Limits from sqlite database

limit_dict = read_trader_limits(con)

####### Check policies

breaches = []

# Policy 1: long/short consideration: max amount in USD$ (mark to market) for a single stock position;
print("# Checking Policy 1: long/short consideration")
breaches += check_policy1(df, limit_dict)

# Policy 2: volume relative (%): max position given daily volumes. 
print("# Checking Policy 2: volume relative")
breaches += check_policy2(df, limit_dict)

# Policy 3: sector relative: max concentration of portfolio allocations by sector. 
print("# Checking Policy 3: Sector relative")
breaches += check_policy3(df, limit_dict, con)

# Policy 4: ES relative
print("# Checking Policy 4: ES relative")
breaches += check_policy4_historical(df, limit_dict, con)

# Policy 5: Volatility (%)
print("# Checking Policy 5: Volatility")
breaches += check_policy5(df, limit_dict, con)

# Policy 6: VaR Relative
print("# Checking Policy 6: VaR Relative")
breaches += check_policy6_historical(df, limit_dict, con)


##### Save to the sqlite database
print("# Saving the breaches to the sqlite database...")
save_breaches(breaches, con, recreate=recreate_policy_breaches)
print("Saved\n")

##### compute portfolios for trader JBX1566
TRADER = "JBX1566"
compute_portfolios(df, trade_type_df, TRADER, con)


##### Close database connections
con.close()
client.close()
