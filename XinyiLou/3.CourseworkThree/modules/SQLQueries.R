#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Xinyi Lou
# Topic   : create sql query 
#--------------------------------------------------------------------------------------


# SQL Query to load Static Bond ----------------------------------------

SQLQueryStatic <- paste0("SELECT * FROM equity_static;")

SQLQuery <- paste0("SELECT * FROM portfolio_positions LEFT JOIN equity_prices ON portfolio_positions.symbol=equity_prices.symbol_id where portfolio_positions.cob_date=equity_prices.cob_date ORDER BY 'cob_date';")