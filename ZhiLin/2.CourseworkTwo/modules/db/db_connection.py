import pymongo
from pymongo import MongoClient
import sqlite3




def sql_connection(sql_name):
    connection = sqlite3.connect(sql_name)
    cursor = connection.cursor()
    return connection,cursor

def mongo_connection(db_name, collection_name, url):
    client_mongo = pymongo.MongoClient(url)
    db_mongo = client_mongo[db_name]
    collection_mongo = db_mongo[collection_name]
    return collection_mongo

