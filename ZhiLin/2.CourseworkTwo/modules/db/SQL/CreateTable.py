
import sqlite3
from sqlalchemy import create_engine
def create_sql_table(path,trades_suspects):
    engine = create_engine(path) 
    trades_suspects.to_sql('oldtable',con=engine, if_exists='replace',index=False)
    # cursor.executescript('''
    # PRAGMA foreign_keys=on;
    # BEGIN TRANSACTION;
    # CREATE TABLE trades_suspects (Date_Time TEXT PRIMARY KEY NOT NULL,
    #                               TradeId TEXT NOT NULL,
    #                               Trader TEXT NOT NULL,
    #                               symbol_id TEXT NOT NULL,
    #                               Quantity INTEGER NOT NULL,
    #                               Notional REAL NOT NULL,
    #                               TradeType TEXT NOT NULL,
    #                               Ccy TEXT NOT NULL,
    #                               Counterparty TEXT NOT NULL,
    #                               price_id TEXT NOT NULL,
    #                               FOREIGN KEY(price_id) REFERENCES equity_prices(price_id));

    # INSERT INTO trades_suspects SELECT * FROM oldtable;

    # DROP TABLE oldtable;
    # COMMIT TRANSACTION;''')
    
    #In order to set primary and foreign keys, a new table is created.
    #The data from "oldtable" which is a table created by the dataframe named trades_suspects, 
    #has been inserted into the new table called "trades_suspects".
    #This block of code has been moved to main.py and the exact same effect can be achieved.
