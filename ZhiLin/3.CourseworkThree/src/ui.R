#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author     : Zhi Lin
# Coursework : Coursework3
# Use Case   : Incorrect Trade Detection
# File       : ui.R
#--------------------------------------------------------------------------------------
# Import Libraries
library(DT)
library(shinydashboard)
library(shinythemes)
library(ggplot2)
library(lubridate)
library(dplyr)
library(RSQLite)   
library(mongolite)
library(dplyr)
library(tibble)
library(shiny)
library(lubridate)
options(scipen = 999)

# Source config
source(paste0("./ZhiLin/3.CourseworkThree/config/","script.config"))
# Source params
source(paste0("./ZhiLin/3.CourseworkThree/config/","script.params"))

# Create SQL Database connection
ConSql <- dbConnect(RSQLite::SQLite(), DBConnection$SQLDataBase$Connection)
# MongoDB connection
ConMongo <- mongo(DBConnection$MongoDataBase[[1]],DBConnection$MongoDataBase[[2]],DBConnection$MongoDataBase[[3]])

# Retrieve Data From MongoDB ------------------------------------------------------------------------------------------------------------------------------------------

# Define Trader
trader <- ConMongo$aggregate('[{"$group":{"_id":"$Trader"}}]')
colnames(trader) <- c("trader")
trader$trader <- sort(trader$trader)

# cob_date
cob_date <- unique(RSQLite::dbGetQuery(ConSql,"SELECT cob_date FROM portfolio_positions"))
cob_date$cob_date <- lubridate::dmy(cob_date$cob_date)
cob_date$cob_date <- format(cob_date$cob_date,'%Y-%m-%d')

cob_date <- rbind(cob_date,"2021-11-11")
cob_date <- rbind(cob_date,"2021-11-12")

header <- dashboardHeader(title = "Daily Positions and Exposures by Trader",titleWidth = 500,
                          dropdownMenuOutput("msg_menu"),
                          dropdownMenuOutput("tsk_menu"))

sidebar <- dashboardSidebar(
  div(htmlOutput("welcome"), style = "padding: 20px"),
  sidebarMenu(
    menuItem("View Position",tabName = "view_position",icon = icon("search")),
    menuItem("View Exposures",tabName = "view_exposure",icon = icon("exchange-alt"))
  ),
  selectizeInput('id', 'Select trader id', trader, selected = NULL),
  selectizeInput('date', 'Select trade date',cob_date, selected = NULL)
)

body <-dashboardBody(
  tabItems(
    tabItem(tabName = "view_position",uiOutput("tab1UI")),
    tabItem(tabName = "view_exposure",uiOutput("tab2UI"))
  )
)

ui <- dashboardPage(header,sidebar,body)