# screePlot function

# 2021-11-11
# Determine the no. of clusters by using scree plot
wss11 <- (nrow(mergePrice11[,c("impliedPrice", "close")])-1)*sum(apply(mergePrice11[,c("impliedPrice", "close")],2,var))
# Calculate total within sum of squares for 1 to 15 cluster centers
for (i in 2:15) wss11[i] <- sum(kmeans(mergePrice11[,c("impliedPrice", "close")], 
                                       centers=i)$withinss)
# Plot total within sum of squares vs. number of clusters (scree plot)
plot(1:15, wss11, type="b", xlab="Number of Clusters",
     ylab="Within groups sum of squares")
