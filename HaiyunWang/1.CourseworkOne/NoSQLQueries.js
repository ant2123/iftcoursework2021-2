//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
// UCL -- Institute of Finance & Technology
// Author: Haiyun Wang
// Coursework One: NoSQL DataBase - MongoDB
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

// 0. Load the data
use Equity



// 1. Browse the data:
// This confirms that the Equity database is a snapshot of S&P 500 component stocks.

// 1.1 Count the total number of documents
db.CourseworkOne.find().count()

// 1.2 Browse the first and last 5 documents
db.CourseworkOne.find().limit(5)
db.CourseworkOne.find().skip(500).limit(5)



// 2. Find stocks with different level of Marketcap

// 2.1 Large-cap: >= $10 billion, i.e. >= $10,000M
db.CourseworkOne.find({"MarketData.MarketCap": {$gte: 10000}}).count()

// 2.2 Mid-cap: $2 billion - $10 billion, i.e. ($2,000M, $10,000M)
db.CourseworkOne.find({"MarketData.MarketCap": {$gt: 2000, $lt: 10000}}).count()

// 2.3 Small-cap: $300 million - $2 billion, i.e. ($300M, $2,000M]
db.CourseworkOne.find({"MarketData.MarketCap": {$gt: 300, $lte: 2000}}).count()

// 2.4 Micro-cap: $50 million - $300 million, i.e. [$50M, $300M]
db.CourseworkOne.find({"MarketData.MarketCap": {$gte:50, $lte: 300}}).count()

// 2.5 NULL
db.CourseworkOne.find({"MarketData.MarketCap": null}).count()



// 3. Beta-related analysis

// 3.0 Preliminary: Use distinct method to see the GICS Sector that these stocks cover
db.CourseworkOne.distinct("StaticData.GICSSector")
db.CourseworkOne.distinct("StaticData.GICSSubIndustry")



// 3.1 Classify cyclical and defensive stocks by beta:
// Criteria: Beta > 1 -> Cyclicals; Noncyclicals otherwise
db.CourseworkOne.find({"MarketData.Beta": {"$gt": 1}})
db.CourseworkOne.find({"MarketData.Beta": {"$lt": 1}})

// 3.2 Aggregate all GICS sectors and calculate the average beta of each sector. Sort the output in descending order -- the higher the average beta, the more cyclical a sector will be. 
db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector",
		 avg_beta: {$avg: "$MarketData.Beta"} } }
]).sort({avg_beta: -1})



// 4. An imaginary scenario of query
// Recommend stocks for risk averse investors.
// Criteria: large-cap (>$10000M), low beta (<1), and moderate payout ratio (<50)
var cursor = db.CourseworkOne.find({$and: [
					{"MarketData.MarketCap": {$gt: 10000}}, 
					{"MarketData.Beta": {$lt: 1}},
					{"FinancialRatios.PayoutRatio": {$lt: 50}}
					]}, 
				{_id: 0,
				Symbol: 1,
				"FinancialRatios.PERatio": 1, 
				"FinancialRatios.DividendYield": 1});
cursor.forEach(printjson)



/*
The following queries are voluntary.
Corresponding query documentation is therefore not provided in the report due to word limits.
*/

// 5. Dividend Yield
// Aggregate all GICS sectors and calculate the average dividend yield of each sector. Sort the output in descending order.
// Findings: Real Estate, Energy, and Utilities on average have high dividend yields, whereas Health Care, Information Technology, and Communication Services, which require reinvestment and continuing growth, distribute less dividends on average.
db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector", 
		 avg_DY: {$avg: "$FinancialRatios.DividendYield"} } }
]).sort({avg_DY: -1})


// 6. Payout Ratio
// Find out the companies whose Payout Ratios are greater than 100%. Their sustainability is worth questioning.
db.CourseworkOne.find({"FinancialRatios.PayoutRatio": {"$gt": 100}})



// 7. PE Ratio
// Aggregate all GICS sectors and calculate the average PE ratio of each sector. Sort the output in ascending order. 
// Since looking at one PE ratio alone is meaningless, it is useful to compute the average PE ratio of each sector as a benchmark when making comparisons.
db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector", 
		 avg_PE: {$avg: "$FinancialRatios.PERatio"} } }
]).sort({avg_PE: 1})
