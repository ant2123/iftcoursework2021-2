import motor.motor_asyncio

def mongo_connection(conn_str, database, collection):
    client = motor.motor_asyncio.AsyncIOMotorClient(conn_str)

    db = client[database]
    coll = db[collection]

    return coll
# mongo_connection('mongodb://localhost:27017', "Equity", "CourseworkTwo")