import numpy as np
import pandas as pd
import sys
sys.path.append("..")
sys.path.append("..")
from test.input_tests import(
    find_zcore,
    find_irq,
    find_dbscan
)

def merge_suspects(historical_data):
    trades_suspects = pd.DataFrame()

    trades_suspects = trades_suspects.append(find_zcore(historical_data))
    trades_suspects = trades_suspects.append(find_irq(historical_data))
    trades_suspects = trades_suspects.append(find_dbscan(historical_data))

    return trades_suspects.drop_duplicates().values
