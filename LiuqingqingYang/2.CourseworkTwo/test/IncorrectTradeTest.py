#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./test/IncorrectTradeTest.py
#--------------------------------------------------------------------------------------

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn import cluster

# detect incorrect trades using z-score
def find_zcore(data, column, date):   
    
    data = data[data['DateTimeStamp']<=pd.to_datetime(date)]  
    data_zscore = data.copy()[column]
    
    z_score = (data_zscore - data_zscore.mean()) / data_zscore.std()
    data_zscore = z_score.abs() > 3.0
    
    outliers_all = data[data_zscore == True]

    return outliers_all.loc[outliers_all['DateTime'].str.contains(date)]

# detect incorrect trades using interquartile range method 
def find_irq(data, column, date):
    
    data = data[data['DateTimeStamp']<=pd.to_datetime(date)]
    outliers_all = pd.DataFrame()
    
    data_std = np.std(data[column])
    data_mean = np.mean(data[column])
    lower_limit = data_mean - data_std*3
    upper_limit = data_mean + data_std*3
    
    for i in data.index:
        quantity = data.loc[i][column]
        if (quantity < lower_limit)|(quantity > upper_limit):
            outliers_all = outliers_all.append(data.loc[[i]])

    return outliers_all.loc[outliers_all['DateTime'].str.contains(date)]

# detect incorrect trades using dbscan
def find_dbscan(data, column, date):
    
    data = data[data['DateTimeStamp']<=pd.to_datetime(date)]    
    data_dbscan = np.array(preprocessing.scale(data[column])).reshape(-1,1)
    
#     dbscan = cluster.DBSCAN(eps=.3)
    dbscan = cluster.DBSCAN(eps = 0.8, min_samples = 7)
    dbscan.fit(data_dbscan)
    
    outliers_all = pd.DataFrame()
    outliers_all = outliers_all.append(data)
    outliers_all['Cluster'] = dbscan.labels_

    outliers_all = outliers_all[outliers_all['Cluster']==-1].drop(['Cluster'], axis=1)
    
    return outliers_all.loc[outliers_all['DateTime'].str.contains(date)]