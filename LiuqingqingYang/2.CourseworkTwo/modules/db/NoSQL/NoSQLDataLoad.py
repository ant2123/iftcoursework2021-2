#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/db/NoSQL/NoSQLDataLoad.py
#--------------------------------------------------------------------------------------

import pymongo
import pandas as pd

# connect to mongodb
def connect_mongo(host='localhost', port=27017, username=None, password=None):
    if username==None:
        conn = pymongo.MongoClient(host, port)
    else:
        mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (username, password, host, port)
        conn = pymongo.MongoClient(mongo_uri)
    return conn

# read from mongodb and store in DataFrame
def read_mongo(conn, db, coll, query=None, no_id=False): 
    db = conn[db]
    coll = db[coll]
    
    if 'group' in str(query):
        cursor = coll.aggregate(query)
        df = pd.DataFrame(list(cursor))
    else:  
        cursor = coll.find(query)
        df =  pd.DataFrame(list(cursor))   
   
    return df