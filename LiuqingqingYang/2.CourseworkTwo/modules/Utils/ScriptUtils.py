#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/Utils/ScriptUtils.py
#--------------------------------------------------------------------------------------

import configparser
import importlib

# read script.config
def read_config():
    conf = configparser.ConfigParser()
    conf.read(".\config\script.config")
    sql_path = conf.get("config","sql_path")
    database = conf.get("config","database")
    collection =conf.get("config","collection")
    return sql_path, database, collection

# read script.params
def read_params():
    conf = configparser.ConfigParser()
    conf.read(".\config\script.params")
    detect_columns = eval(conf.get("params","detect_columns"))
    detect_dates = eval(conf.get("params","detect_dates"))
    return detect_columns, detect_dates