#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/Utils/PortFolioUtils.py
#--------------------------------------------------------------------------------------

import pandas as pd
import time

import modules.db.NoSQL.NoSQLDataLoad as nsdl
import modules.db.SQL.SQLDataLoad as sdl

# to get aggregate query results for portofolio_position
def get_portfolio_agg(conn, db, coll):
    query =[{"$match": {} },
            {"$group": {"_id":{"cob_date":"$DateTime", "trader":"$Trader", "symbol":"$Symbol", "ccy":"$Ccy"}, 
                         "net_quantity": {"$sum": "$Quantity"} , "net_amount": {"$sum": "$Quantity"} 
                       } 
            }]
    df = nsdl.read_mongo(conn, db, coll, query)

    return df

# to transform nosql aggregate query results for portofolio_position
def transform_portfolio_agg(data):
    pos_id = []
    cob_date = []
    trader = []
    symbol = []
    ccy = []
    df = pd.DataFrame()
    for i, row in data.iterrows():
        pos_id.append(row['_id']['trader']+time.strftime("%Y%m%d", time.strptime(row['_id']['cob_date'][8:18],"%Y-%m-%d"))+row['_id']['symbol'])
        cob_date.append(time.strftime("%d-%b-%Y", time.strptime(row['_id']['cob_date'][8:18],"%Y-%m-%d")))
        trader.append(row['_id']['trader'])
        symbol.append(row['_id']['symbol'])
        ccy.append(row['_id']['ccy'])
    df['pos_id'] = pos_id
    df['cob_date'] = cob_date
    df['trader'] = trader
    df['symbol'] = symbol
    df['ccy'] = ccy
    df = pd.concat([df, data[['net_quantity','net_amount']]], axis=1)
    return df

# find new portfolio_positions that exists in mongodb and has not been written in sqlite
def new_portfolio(conn_sql, conn_nosql,db, coll):
    
    portfolio_nosql_agg = get_portfolio_agg(conn_nosql, db, coll)
    portfolio_nosql = transform_portfolio_agg(portfolio_nosql_agg)
    
    query_sql = "SELECT * FROM portfolio_positions"
    portfolio_sql = sdl.read_sqlite(conn_sql, query_sql)
    
    diff_pos_id = pd.concat([portfolio_nosql[['pos_id']],
                             portfolio_sql[['pos_id']], 
                             portfolio_sql[['pos_id']]]).drop_duplicates(keep=False)
    df = pd.merge(portfolio_nosql, diff_pos_id)  
    
    return df