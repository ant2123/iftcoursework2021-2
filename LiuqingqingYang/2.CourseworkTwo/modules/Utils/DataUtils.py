#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Liuqingqing Yang
# Course  : CEGE0069 - Big Data in Quantitative Finance
# Topic   : Coursework Two - incorrect trade detection 
# File    : ./modules/Utils/DataUtils.py
#--------------------------------------------------------------------------------------

import pandas as pd
import time

# calculate trade price
def calculate_trade_price(data, colname):
    data[colname] = data['Notional']/data['Quantity']
    return data

# calculate time stamp for DateTime
def calculate_date_time_stamp(data, column):
    DateTimeStamp = []
    for i in range(len(data)):
        DateTimeStamp.append(pd.to_datetime(data['DateTime'][i][8:18]))
    data[column] = DateTimeStamp
    return data

# calculate time str for DateTime
def calculate_date_time_str(data, column):
    DateTimeStr = []
    for i in range(len(data)):       
        DateTimeStr.append(time.strftime("%Y%m%d", time.strptime(data['DateTime'][i][8:18],"%Y-%m-%d")))
    data[column] = DateTimeStr
    return data