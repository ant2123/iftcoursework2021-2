import os
import pickle
from cryptography.fernet import Fernet


def read(filename):
    with open(filename, "rb") as file:
        bin_data = file.read()
    return bin_data


def write(filename, data):
    with open(filename, "wb") as file:
        file.write(data)

def get_key(filename):
    '''load key for encryption/decryption'''
    key = read(filename)
    return pickle.loads(key)


def generate_key(filename):
    key = Fernet.generate_key()
    key = pickle.dumps(key)
    write(filename, key)


def read_encrypted(filename, key):
    bin_data = read(filename)
    fnt_decryptor = Fernet(key)
    dcr_data = fnt_decryptor.decrypt(bin_data)
    file_data = pickle.loads(dcr_data)
    return file_data

def write_encrypted(dictionary_cr, key, filename):
    fnt_encrypted = Fernet(key)
    pkl_dictionary = pickle.dumps(dictionary_cr)
    encr_dictionary = fnt_encrypted.encrypt(pkl_dictionary)
    write(filename, encr_dictionary)



