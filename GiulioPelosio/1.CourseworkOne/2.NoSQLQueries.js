//Create a variable to store average of MarketCap for the Utilities sector 

var average_one = db.CourseworkOne.aggregate([
	{$group: {_id: "$StaticData.GICSSector.Utilities", avg: {$avg: "$MarketData.MarketCap"} } }
    ]).toArray()[0]["avg"];

//Find companies within the Utilities sector with a PERatio below the average sector in 2020 and the market cap grater than the average of the sector

db.CourseworkOne.find({$and: [ { "MarketData.MarketCap": { "$gt": "average_one" }},  
                               {"FinancialRatios.PERatio": {"$gt": 1, "$lte": 29}}, 
                               {"StaticData.GICSSector": {"$eq": "Utilities" } } ] } )

//Create an overview of the average of financial indicators and ratios for each sector 

    db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector", 
    Total_MarketCap: {$sum: "$MarketData.MarketCap"},
    Average_MarketCap: {$avg: "$MarketData.MarketCap"},
    Average_Beta: {$avg: "$MarketData.Beta"},
    Average_DividendYield: {$avg: "$FinancialRatios.DividendYield"}, 
    Average_PERatio: {$avg: "$FinancialRatios.PERatio"}, 
    Average_PayoutRatio: {$avg: "$FinancialRatios.PayoutRatio"} } },
    {$sort:  {Total_MarketCap: -1}}
    ])
