import matplotlib.pyplot as plt
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import json

# open connectivity to MongoDB
con = MongoClient('mongodb://localhost')
db = con.Equitydb
col = db.CourseworkTwo


# MongoDB Query
all_data = col.find({}) 
df = pd.DataFrame(all_data)
df.to_excel('result/RAWDATA.xlsx',index=None)

print(df.head())
print(df.info())

def get_date(s):
    return s[8:18]
df['DateTime_date'] = df["DateTime"].apply(get_date)

"""
2021-11-11
"""
df1 = df[df['DateTime_date']=='2021-11-11']

model_kmeans = KMeans(n_clusters=5,init="k-means++",max_iter=500,random_state=20)
model_kmeans.fit(df1[['Quantity','Notional']])

# visualise data
df1['cluster'] = model_kmeans.labels_
center = pd.DataFrame(model_kmeans.cluster_centers_)
# print(center)

# plot results
plt.figure(figsize=(15,7))
plt.scatter(df1.loc[:,'Quantity'], df1.loc[:,'Notional'],c=model_kmeans.labels_)
plt.scatter(center.iloc[:,0],center.iloc[:,1],c=range(model_kmeans.n_clusters),marker='x')
plt.scatter(center.iloc[:,0],center.iloc[:,1],c=range(model_kmeans.n_clusters),alpha=0.3,s=2000)
plt.savefig('result/2021-11-11_ClusteringResult.png')
plt.show()

# print(df1['cluster'].value_counts())
df1.to_excel('result/2021-11-11_kmeans_ClusteringResult.xlsx', index=None)  
 
df2 = df1.groupby(['cluster'])['Quantity'].count().reset_index()
df3 = df2[df2['Quantity']==1]
cluster_val = list(df3['cluster'])
df_res = pd.DataFrame()
for cl in cluster_val:
    df4 = df1[df1['cluster']==cl]
    print(df4, df4['Quantity'])
    df_res.append(df4)
df_res.to_excel('result/2021-11-11 kmeans_AbnormalResults.xlsx', index=None)  
 
"""
2021-11-12
"""
df1 = df[df['DateTime_date']=='2021-11-12']

model_kmeans = KMeans(n_clusters=8,init="k-means++",max_iter=500,random_state=20)
model_kmeans.fit(df1[['Quantity','Notional']])

# visualise data
df1['cluster'] = model_kmeans.labels_
center = pd.DataFrame(model_kmeans.cluster_centers_)
# print(center)

# plot results
plt.figure(figsize=(15,7))
plt.scatter(df1.loc[:,'Quantity'], df1.loc[:,'Notional'],c=model_kmeans.labels_)
plt.scatter(center.iloc[:,0],center.iloc[:,1],c=range(model_kmeans.n_clusters),marker='x')
plt.scatter(center.iloc[:,0],center.iloc[:,1],c=range(model_kmeans.n_clusters),alpha=0.3,s=2000)
plt.savefig('result/2021-11-12_ClusteringResult.png')
plt.show()
df1['cluster'].value_counts()

df1.to_excel('result/2021-11-12 kmeans_ClusteringResult.xlsx', index=None)  
 
df2 = df1.groupby(['cluster'])['Quantity'].count().reset_index()
df3 = df2[df2['Quantity']==1]
cluster_val = list(df3['cluster'])
df_res = pd.DataFrame()
for cl in cluster_val:
    df4 = df1[df1['cluster']==cl]
    print(df4, df4['Quantity'])
    df_res.append(df4)
df_res.to_excel('result/2021-11-12 kmeans_AbnormalResults.xlsx', index=None) 

con.close()