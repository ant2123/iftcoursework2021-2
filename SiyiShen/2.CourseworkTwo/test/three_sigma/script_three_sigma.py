import matplotlib.pyplot as plt
from pymongo import MongoClient
import pandas as pd

# open connectivity to MongoDB
con = MongoClient('mongodb://localhost')
db = con.Equitydb
col = db.CourseworkTwo


# MongoDB Query
all_data = col.find({}) 
df = pd.DataFrame(all_data)

print(df.head())
print(df.info())

def get_date(s):
    return s[8:18]
df['DateTime_date'] = df["DateTime"].apply(get_date)


def three_sigma(col):
    low = col.mean()-3*col.std()
    up = col.mean()+3*col.std()
    drop_index = col.loc[(col < low) | (col > up)].index
    for i in drop_index:
        print(col[i])
    return drop_index, len(drop_index)


for date_str in ['2021-11-11', '2021-11-12']:
    df1 = df[df['DateTime_date']==date_str]
    for col in ['Quantity','Notional']:
        sigma_res = three_sigma(df1[col])
        # 异常
        df_res = df1.loc[sigma_res[0]]
        df_res.to_excel('result/%s %s three_sigma.xlsx' % (date_str, col))

con.close()