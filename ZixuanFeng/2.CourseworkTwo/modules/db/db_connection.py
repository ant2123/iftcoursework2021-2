#!/usr/bin/env python
# coding: utf-8

# In[6]:


import pymongo
import sqlite3


# In[5]:


def mongo_conn(db_name, collection_name, url):
    client = pymongo.MongoClient(url)
    db = client[db_name]
    collection = db[collection_name]
    return collection


# In[7]:


def sql_conn(db_name):
    conn=sqlite3.connect(db_name)
    c=conn.cursor()
    return conn,c







