from fastapi import APIRouter
from src.routes.trades import router as trades_router

router = APIRouter()
router.include_router(trades_router, prefix="/API_Trades_Input", tags=["API_Trades_Input"])