#Import packages
library(dplyr)
library(lubridate)
library(RSQLite)
library(mongolite)
library(ggplot2)
library(PerformanceAnalytics)

#Set working directory
GITRepoDirectory <- "C:/Users/12619/iftcoursework2021"
source(paste0(GITRepoDirectory, "/YinDuan/2.CourseworkTwo/config/script.config"))
source(paste0(GITRepoDirectory, "/YinDuan/2.CourseworkTwo/config/script.params"))
source(paste0(GITRepoDirectory,Config$Directories$ScriptFolder , "/input/input_loader.R"))



#Connect to SQLite
conSql <- dbConnect(RSQLite::SQLite(), paste0(GITRepoDirectory, Config$Directories$DBFolder,"/Equity.db"))

#Input the table needed into R
Equity_Price <- as.data.frame(dbGetQuery(conSql,SQLQueryEquityPrice))
Portfolio_Position <- as.data.frame(dbGetQuery(conSql,SQLPortfolioPosition))
Equity_Static <- as.data.frame(dbGetQuery(conSql,SQLEquityStatic))
Trader_Limits <- as.data.frame(dbGetQuery(conSql,SQLQueryTraderlimits))

#Connect to Mongodb
conMongo <- mongo(collection = Params$Database$Mongo$collection, db = Params$Database$Mongo$db, url = Params$Database$Mongo$url,
                  verbose = FALSE, options = ssl_options()) 
Data<- conMongo$find(query = find_all)
head(Data)
Data_main<- conMongo$find(query = find1112)



#Data aggregation
#Aggregate the trades on 2021-11-11 & 2021-11-12 by Trader, Symbol, Date and Ccy
options(scipen=999)
Sys.setlocale("LC_TIME", "English")
source(paste0(GITRepoDirectory,Config$Directories$ScriptFolder , "/Data Preprocessing.R"))
Data_1112

#Retrieve all the policy limits
source(paste0(GITRepoDirectory,Config$Directories$ScriptFolder , "/Policy Limits.R"))
Trader_Limits
TL_available   #All the policy limits available on 2021-11-11 & 2021-11-12 

#This function can help find the threshold of long consideration limits that trader MRH5231 adopted
LimitPolicy('long','MRH5231')  



#1.Long/Short Consideration
source(paste0(GITRepoDirectory,Config$Directories$ScriptFolder , "/policy1.R"))
policy_breaches_long
policy_breaches_short

#2.Sector relative
source(paste0(GITRepoDirectory,Config$Directories$ScriptFolder , "/policy2.R"))
policy_breaches_sector

P_Sector <- ggplot(data = Sector_A,
            mapping = aes(x=Trader,
                          y=amount,
                          fill=GICSSector))+geom_bar(stat='identity',position = 'dodge')+facet_wrap(~ Date)

P_Sector

#3.VaR
source(paste0(GITRepoDirectory,Config$Directories$ScriptFolder , "/policy3.R"))

#2021-11-11
data11_DMZ1796
VaR(data11_DMZ1796$return,p=0.99,method = 'historical')<=-LimitPolicy('VaR','DMZ1796')
data11_JBX1566
VaR(data11_JBX1566$return,p=0.99,method = 'historical')<=-LimitPolicy('VaR','JBX1566')
#2021-11-12
data12_DMZ1796
VaR(data12_DMZ1796$return,p=0.99,method = 'historical')<=-LimitPolicy('VaR','DMZ1796')
data12_JBX1566
VaR(data12_JBX1566$return,p=0.99,method = 'historical')<=-LimitPolicy('VaR','JBX1566')
#All the results are FALSE, so both traders complied with the VaR limits



#Create a new SQL table named policy_breaches
source(paste0(GITRepoDirectory,Config$Directories$ScriptFolder , "/db/SQL/CreateTable.R"))
dbExecute(conSql, Create_Table)

#Aggregate the trades that break the limits 
policy_breaches_long <- left_join(policy_breaches_long,Equity_Static,by=c('Symbol'='symbol'))%>%select(Trader,Symbol,Ccy,Date,Quantity,Notional,amount,GICSSector)
policy_breaches_long$limit_type <-'long' 
policy_breaches_sector2 <- as.data.frame(matrix(nrow=0,ncol = 8))
for (i in 1:nrow(policy_breaches_sector)) {
  sector <- filter(Data1,Data1$Trader==policy_breaches_sector$Trader[i],Data1$Date==policy_breaches_sector$Date[i],Data1$GICSSector==policy_breaches_sector$GICSSector[i])
  policy_breaches_sector2 <- rbind(sector,policy_breaches_sector2)
}
policy_breaches_sector2$limit_type <- 'sector'
policybreaches <- rbind(policy_breaches_long,policy_breaches_sector2)

#Generate the primary key pos_id
policybreaches$pos_id <- paste0(policybreaches$Trader,as.character(format(policy_breaches$Date,'%Y%d%m')),policybreaches$Symbol,policybreaches$limit_type)
policybreaches$Date <- as.character(format(policybreaches$Date,'%d-%b-%Y'))

#Load policybreaches into SQL table policy_breaches
for (i in 1:nrow(policybreaches)) {
  dbExecute(conSql,paste0("INSERT INTO policy_breaches (pos_id, trader, symbol, ccy, date, quantity, notional, amount, GICSSector, limit_type)",
                          "VALUES (\" ",
                          policybreaches$pos_id[i],"\",\"",
                          policybreaches$Trader[i],"\",\"",
                          policybreaches$Symbol[i],"\",\"",
                          policybreaches$Ccy[i],"\",\"",
                          policybreaches$Date[i],"\",",
                          policybreaches$Quantity[i],",",
                          policybreaches$Notional[i],",",
                          policybreaches$amount[i],",\"",
                          policybreaches$GICSSector[i],"\",\"",
                          policybreaches$limit_type[i],"\")"
  ))
}



#Generate cob_date and pos_id
Data_aggregate <- Data_1112
Data_aggregate$cob_date <- as.character(format(Data_aggregate$Date,'%d-%b-%Y'))
Data_aggregate$pos_id <- paste0(Data_aggregate$Trader,as.character(format(Data_aggregate$Date,'%Y%d%m')),Data_aggregate$Symbol)

#Load all trades into portfolio_positions
for (i in 1:nrow(Data_aggregate)) {
  dbExecute(conSql,paste0("INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)",
                          "VALUES (\" ",
                          Data_aggregate$pos_id[i],"\",\"",
                          Data_aggregate$cob_date[i],"\",\"",
                          Data_aggregate$Trader[i],"\",\"",
                          Data_aggregate$Symbol[i],"\",\"",
                          Data_aggregate$Ccy[i],"\",",
                          Data_aggregate$Quantity[i],",",
                          Data_aggregate$Notional[i],")"
                          ))
}



#Close the connection
dbDisconnect(conSql)
conMongo$disconnect()
