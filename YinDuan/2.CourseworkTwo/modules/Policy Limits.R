#--------------------------------------------------------------------------------------
# Coursework2
# Author  : Yin Duan
# Topic   : Policy Limits
#--------------------------------------------------------------------------------------

#Keep the rows whose limit_end is 'NA'
TL_available<- filter(Trader_Limits,is.na(Trader_Limits$limit_end))


for (i in 1:nrow(TL_available)) {
  TL_available$limit_amount[i] <- as.numeric(TL_available$limit_amount[i])
  if(TL_available$limit_category[i]=='relative'){
    TL_available$limit_amount[i] <- TL_available$limit_amount[i]/100
  }
}
#This function is to find the threshold of a limit policy for a specific trader 
LimitPolicy <- function(limit,trader){
  filter(TL_available,TL_available$limit_type==limit,TL_available$trader_id==trader)$limit_amount
}
