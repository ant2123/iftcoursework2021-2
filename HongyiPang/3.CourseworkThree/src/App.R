#--------------------------------------------------------------------------------------
# CW3
# Author: Hongyi Pang
# Topic: Run application 
#--------------------------------------------------------------------------------------

library(dplyr)
library(lubridate)
library(RSQLite)
library(quantmod)
library(cvar)
library(mongolite) 
library(ggplot2)
library(shiny)
library(shinydashboard)

#Args = c("C:/Users/phy0901/Desktop/iftcoursework2021", "script.config", "script.params")
Args <- commandArgs(TRUE)

# Set working directory
setwd(Args[1]) 

# Set other args
MyArgs = c("server.R", "ui.R")
# Source needed files
source(paste0("./HongyiPang/3.CourseworkThree/config/", Args[2]))
# Source params
source(paste0("./HongyiPang/3.CourseworkThree/config/", Args[3]))

source(paste0("./HongyiPang/3.CourseworkThree/src/", MyArgs[1]))
source(paste0("./HongyiPang/3.CourseworkThree/src/", MyArgs[2]))
#-------------------------------------------------------------------------------------------

# Run the application 
shinyApp(ui = ui, server = server)















