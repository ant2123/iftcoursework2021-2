
#%% Create Table 
def SQLCreateRetTable():
    Command = "CREATE TABLE trades_suspects (\
    Symbol TEXT NOT NULL,\
    Price INTEGER,\
    low INTEGER,\
    high INTEGER,\
    cob_date TEXT,\
    Trader TEXT,\
    FOREIGN KEY (Price) REFERENCES equity_prices (symbol_id))"
    return (Command)
