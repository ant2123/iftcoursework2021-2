# Coursework 2 Use case: incorrect trade detection

## Script
The script performs the following tasks:

1. Retrieve all trades at the end of 2021-11-11 & 2021-11-12; 
2. For each day, check that trades are consistent with expectations; 
3. Load suspect trades into a new table in SQL; 
4. Aggregate Quantity and Notional for all trades by date, trader, symbol, ccy. 

