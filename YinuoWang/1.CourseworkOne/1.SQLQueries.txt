/* 
FIrstly, show the overview of the tables. 
*/ 
SELECT * FROM equity_prices LIMIT 5;
SELECT * FROM equity_static LIMIT 5;
SELECT * FROM portfolio_positions LIMIT 5;

/*
Group by GICSSector to see how many companies are there in each sector. 
Put the numbers in positive order to compare the number of companies included in each sector.
*/
SELECT GICSSector, COUNT(symbol) AS company_number
FROM equity_static
GROUP BY GICSSector ORDER BY  company_number DESC;

/*
GROUP BY query with functions ROUND, SUM, AVG, MIN and MAX. 
in order to summarise the min, max, total and the average volume with the same cob_date. 
Order by sum_volume to compare. 
*/
SELECT cob_date, SUM(volume) AS sum_volume, ROUND(AVG(volume), 1) AS mean_volume, MIN(volume), MAX(volume)
FROM equity_prices WHERE cob_date IS NOT NULL 
GROUP BY cob_date ORDER BY sum_volume DESC 
LIMIT 10;

/*
GROUP BY query with functions ROUND, AVG, MIN and MAX. 
in order to summarise the min, max, and the average volume with the same symbol_id on Nov.12, 2021. 
Calculate the daily return rate of each symbol under that day, and list those larger than 0.01 in order. 
*/
SELECT symbol_id, ROUND(AVG(open), 2) AS mean_open, ROUND(AVG(close), 2) AS mean_close, 
MIN(open), MAX(open), MIN(close), MAX(close),
ROUND((close-open)/open, 4) AS daily_return
FROM equity_prices WHERE cob_date = '12-Nov-2021'
GROUP BY symbol_id 
HAVING daily_return > 0.01
ORDER BY daily_return DESC;

/*
Left join  equity_prices and equity_static with the same symbol on Nov.11, 2021.
*/
SELECT equity_prices.symbol_id, equity_prices.open, equity_prices.close, equity_prices.volume, equity_prices.cob_date
FROM equity_prices
LEFT JOIN equity_static ON equity_prices.symbol_id = equity_static.symbol
WHERE equity_prices.cob_date = '11-Nov-2021'



