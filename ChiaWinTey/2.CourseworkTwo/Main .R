#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Chia Win Tey
# Topic   : Coursework Two Main 
#--------------------------------------------------------------------------------------


# Import packages in current environment
library(mongolite) 
library(dplyr)
library(lubridate)
library(RSQLite)

# Setting the shown of number
options(scipen=999)

# Connect with MongoDB
conMongo <- mongo(collection = "CourseworkTwo", db = "Equity", url = "mongodb+srv://Chiawin:<PASSWORD>@cluster0.cdjle.mongodb.net/test",
                  verbose = FALSE, options = ssl_options()) 

# Important to set up directories
GITRepoDirectory <- "/Users/chiawin/Desktop/Coursework/iftcoursework2021" 

# Loading the R Script with the SQL Queries
source(paste0(GITRepoDirectory, "//ChiaWinTey/2.CourseworkTwo/config/script.config"))
source(paste0(GITRepoDirectory, "//ChiaWinTey/2.CourseworkTwo/config/script.params"))
source(paste0(GITRepoDirectory, Config$Directories$ScriptFolder,"/modules/db/db_connection.R"))

# Connecting to SQLite
conSql <- dbConnect(RSQLite::SQLite(), paste0(GITRepoDirectory, Config$Directories$DBFolder,"/Equity.db"))


# Selecting all the database from MongoDB
WholeDATA <- conMongo$find(query = "{}")


#FAT FINGER OF DAY 1 (11 NOV 2021)
# Setting The trade of 11-11-2021 in MongoDB as D1
D1 <- conMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-11T00:00:00.000Z)\", \"$lt\": \"ISODate(2021-11-12T00:00:00.000Z)\"}}")

# Setting the SQLite Query (NAME:Equityprices1) as DataFrame Form(NAME:EP_D1)
EP_D1 <- as.data.frame(dbGetQuery(conSql,Equityprices1))

# Inserting the new column with the result of the price per trade into the dataframe and named the new dataframe as D1NEW
D1NEW <- D1                                
D1NEW$Price_per_trade <- D1$Notional / D1$Quantity   
D1NEW       

# Rename the name of column of "Symbol" in SQLite to "symbol_id"
colnames(EP_D1)[colnames(EP_D1) == 'symbol_id'] <- 'Symbol'

# Merge the dataframe of SQLite and MONGODB and named it D1MERGE 
D1MERGE <- merge( D1NEW , EP_D1 , by= 'Symbol' )

# Testing whether the price per trade of D1 fall within the high and low price of the share of D1 and named it as test1
test1 <- vector(length = 200)
for (i in 1:nrow(D1MERGE)){
  if ((D1MERGE[i,"Price_per_trade"]>=D1MERGE[i,"low"] )& (D1MERGE[i,"Price_per_trade"]<=D1MERGE[i,"high"])){
    test1[i] <- 'TRUE'
  }
  else{
    test1[i] <- 'FALSE'
  }
}

# Combining the result of test1 into Dataframe and named it as D1RESULT
D1RESULT <- cbind(D1MERGE, test1)

# Selecting the row with FALSE outcome out from the dataframe and named it as FFD1 (FAT_FINGER_OF_DAY_1)
FFD1 <- subset(D1RESULT, test1 == "FALSE")

# Selecting fat finger of day 1 based on Mongo DATA and bamed it MFFD1(Mongo_FAT_FINGER_DAY_1)
MFFD1 <- subset(D1, (TradeId == "BMRH5231COF20211111133655")|(TradeId == "BSML1458FITB20211111151159" ))


#FAT FINGER OF DAY 2 (12 NOV 2021)
# Setting The trade of 12-11-2021 in MongoDB as D2
D2 <- conMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-12T00:00:00.000Z)\", \"$lt\": \"ISODate(2021-11-13T00:00:00.000Z)\"}}")

# Setting the SQLite Query (NAME:Equityprices2) as DataFrame Form(NAME:EP_D2)
EP_D2 <- as.data.frame(dbGetQuery(conSql,Equityprices2))

# Inserting the new column with the result of the price per trade into the dataframe and named the new dataframe as D2NEW
D2NEW <- D2                               
D2NEW$Price_per_trade <- D2$Notional / D2$Quantity   
D2NEW

# Rename the name of column of "Symbol" in SQLite to "symbol_id"
colnames(EP_D2)[colnames(EP_D2) == 'symbol_id'] <- 'Symbol'

# Merge the dataframe of SQLite and MONGODB and named it D2MERGE 
D2MERGE <- merge( D2NEW , EP_D2 , by= 'Symbol' )

# Testing whether the price per trade of D2 fall within the high and low price of the share of D2 and named it as test2
test2 <- vector(length = 200)
for (i in 1:nrow(D2MERGE)){
  if ((D2MERGE[i,"Price_per_trade"]>=D2MERGE[i,"low"] )& (D2MERGE[i,"Price_per_trade"]<=D2MERGE[i,"high"])){
    test2[i] <- 'TRUE'
  }
  else{
    test2[i] <- 'FALSE'
  }
}

# Combining the result of test2 into Dataframe and named it as D2RESULT
D2RESULT <- cbind(D2MERGE, test2)

# Selecting the row with FALSE outcome out from the dataframe and named it as FFD2 (FAT_FINGER_OF_DAY_2)
FFD2 <- subset(D2RESULT, test2 == "FALSE")

# Selecting fat finger of day 2 based on Mongo DATA and named it MFFD1(Mongo_FAT_FINGER_DAY_2)
MFFD2 <- subset(D2, (TradeId == "BDMZ1796PCAR20211112094431")|(TradeId == "BDHB1075SBAC20211112131814" ))


# Combining both (MFFD1 & MFFD2) in one table and named it MFF_TOTAL(Mongo_FAT_FINGER_TOTAL)
MFF_TOTAL <- rbind(MFFD1,MFFD2)

# Create Table in SQL
source(paste0(GITRepoDirectory, Config$Directories$ScriptFolder,"/modules/db/SQL/CreateTable.R"))
dbExecute(conSql, SQLCreateTable)
dbWriteTable(conSql,"trades_suspects",MFF_TOTAL, row.names=FALSE, append=TRUE ,overwrite = FALSE)

# AGGREGATING THE DATA OF TRADES AT D1 (11 NOV 2021)
# First need to set a new column as Date in D1 which is used to simply the process of aggregate
D1$Date <- (as.Date("2021-11-11"))

# Second, aggregate the quantity (based on Date,Trader,Symbol and Ccy) and named it D1AQ(DAY_1_AGGREGATE_QUANTITY)
D1AQ<- aggregate(Quantity~Date+Trader+Symbol+Ccy,D1,sum)

# Third, aggregate the notional (based on Date,Trader,Symbol and Ccy) and named it D1AN(DAY_1_AGGREGATE_NOTIONAL)
D1AN<- aggregate(Notional~Date+Trader+Symbol+Ccy,D1,sum)

# Then, Combine both the result of aggregate of quantity and notional in one and named it D1F(DAY_1_PRE_FINAL)
D1PF <- left_join(D1AQ, D1AN)

# After that, insert a new column pos_id
D1PF$pos_id <- paste0(D1PF$Trader,20211111, D1PF$Symbol)

# Lastly, seelect the column we need to insert into the SQLite Database and named it D1FV (DAY_1_FINAL_VERSION)
D1FV <- select(D1PF,pos_id,Date,Trader,Symbol,Ccy,Quantity,Notional)

# AGGREGATING THE DATA OF TRADES AT D2 (12 NOV 2021)
# First need to set a new column as Date in D2 which is used to simply the process of aggregate
D2$Date <- (as.Date("2021-11-12"))

# Second, aggregate the quantity (based on Date,Trader,Symbol and Ccy) and named it D2AQ(DAY_2_AGGREGATE_QUANTITY)
D2AQ<- aggregate(Quantity~Date+Trader+Symbol+Ccy,D2,sum)

# Third, aggregate the notional (based on Date,Trader,Symbol and Ccy) and named it D2AN(DAY_2_AGGREGATE_NOTIONAL)
D2AN<- aggregate(Notional~Date+Trader+Symbol+Ccy,D2,sum)

# Then, Combine both the result of aggregate of quantity and notional in one and named it D2F(DAY_2_PRE_FINAL)
D2PF <- left_join(D2AQ, D2AN)

# After that, insert a new column pos_id 
D2PF$pos_id <- paste0(D2PF$Trader,20211112, D2PF$Symbol)

# Lastly, seelect the column we need to insert into the SQLite Database and named it D2FV (DAY_2_FINAL_VERSION)
D2FV <- select(D2PF,pos_id,Date,Trader,Symbol,Ccy,Quantity,Notional)


# Combining both D1FV and D2FV into one table and named it as FV(FINAL_VERSION)  
FV <- rbind(D1FV,D2FV)

# Renaming the coloumn name of FV to fulfill with the portfolio_position in SQLite Database
colnames(FV)[colnames(FV) == 'Date'] <- 'cob_date'
colnames(FV)[colnames(FV) == 'Trader'] <- 'trader'
colnames(FV)[colnames(FV) == 'Symbol'] <- 'symbol'
colnames(FV)[colnames(FV) == 'Ccy'] <- 'ccy'
colnames(FV)[colnames(FV) == 'Quantity'] <- 'net_quantity'
colnames(FV)[colnames(FV) == 'Notional'] <- 'net_amount'

# Changing the format of the cob_date to make it match with the portfolio_position in SQLite Database
FV$cob_date <- format(FV$cob_date, format="%d-%b-%Y")

# Inserting the FV into the portfolio_position in SQLite Database
dbWriteTable(conSql,"portfolio_positions",FV, row.names=FALSE, append=TRUE)

# Close the connection with MongoDB and SQLite
dbDisconnect(conSql)
conMongo$disconnect()