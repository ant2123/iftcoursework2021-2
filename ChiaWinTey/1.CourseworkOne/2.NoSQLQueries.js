//Query 1 - Find out the performance of the company which are invested by the portfolio holder.

db.CourseworkOne.find({$or:[{Symbol:{"$eq":"ORCL"}},{Symbol:{"$eq":"ACN"}},{Symbol:{"$eq":"AMAT"}},{Symbol:{"$eq":"AVGO"}},
{Symbol:{"$eq":"AAPL"}},{Symbol:{"$eq":"IBM"}},{Symbol:{"$eq":"NVDA"}}]})

//Query 2 - Find the average PE ratio of the whole industry to compare the performance of the company.

db.CourseworkOne.aggregate([ {$match: {"StaticData.GICSSector": "Information Technology"} }, {$group: {_id: "$StaticData.GICSSector", AveragePERatio: {$avg: "$FinancialRatios.PERatio"} } }])

//Query 3 - Find the average Payout ratio of the whole industry to compare the performance of the company.

db.CourseworkOne.aggregate([ {$match: {"StaticData.GICSSector": "Information Technology"} }, {$group: {_id: "$StaticData.GICSSector", AveragePayoutRatio: {$avg: "$FinancialRatios.PayoutRatio"} } }])

//Query 4 - Find the average Beta of the whole industry to compare the performance of the company.

db.CourseworkOne.aggregate([ {$match: {"StaticData.GICSSector": "Information Technology"} }, {$group: {_id: "$StaticData.GICSSector", AverageBeta: {$avg: "$MarketData.Beta"} } }])