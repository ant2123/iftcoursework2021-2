client = pymongo.MongoClient("mongodb://localhost:27017/")
 
#Database Name
db = client["Equity"]
 
#Collection Name    
db.list_collection_names()


# In[15]:


col = db["CourseworkTwo"]


# In[16]:


#Loading the MongoDB Database to Python
trade_collection = db.get_collection("CourseworkTwo")
with open("/Users/parinamehta/Desktop/iftcoursework2021/000.DataBases/NoSQL/CourseworkTwo.json") as f:
    file_data = json.load(f)


# In[17]:


trade_collection.insert_many(file_data)


# In[22]:


#Checking how many documents in JSON File
len(file_data)


# In[23]:


df = pd.DataFrame(list(col.find()))
df.head()
display (df)


# In[8]:


#Ensuring data is loaded correctly
file_data
