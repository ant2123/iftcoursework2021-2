#Question 1.a to extract all trades on 2021-11-11
info =  col
filter1={
    'DateTime': {
    "$gte": 'ISODate(2021-11-11T00:00:00.000Z)',
    "$lt" :'ISODate(2021-11-11T23:59:59.000Z)'
    }
}
trades_date1 = info.find(filter=filter1)
trades_date1_df =  pd.DataFrame(list(trades_date1))
display(trades_date1_df)


# In[25]:


#Question 1.b to extract all trades on 2021-11-12
filter2={
    'DateTime': {
    "$gte": 'ISODate(2021-11-12T00:00:00.000Z)',
    "$lt" :'ISODate(2021-11-12T23:59:59.000Z)'
    }
}

trades_date2 = info.find(filter=filter2)
trades_date2_df3 =  pd.DataFrame(list(trades_date2))
display(trades_date2_df3)
