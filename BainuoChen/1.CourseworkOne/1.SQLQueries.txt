SELECT GICSSector,
AVG(volume) AS AVG_volume
FROM equity_prices INNER JOIN equity_static ON (equity_prices.symbol_id = equity_static.symbol)
WHERE equity_prices.cob_date = '12-Nov-2021'
GROUP BY GICSSector
ORDER BY AVG_volume DESC;



SELECT symbol_id, ABS(high - low) AS Difference
FROM equity_prices
WHERE equity_prices.cob_date = '12-Nov-2021'
ORDER BY Difference DESC;