db.CourseworkOne.aggregate([
	{$match: {} },
	{$group: {_id: "$StaticData.GICSSector", total_MarketCap: {$sum: "$MarketData.MarketCap"} } },
    {$sort:{total_MarketCap:-1}}
])



db.CourseworkOne.find({$and:[{"MarketData.MarketCap":{"$gt":60000}},{"MarketData.Beta":{"$lt":0.8}}, {"FinancialRatios.DividendYield":{"$gt":3}} ]}).sort({"FinancialRatios.DividendYield":-1}).limit(5)