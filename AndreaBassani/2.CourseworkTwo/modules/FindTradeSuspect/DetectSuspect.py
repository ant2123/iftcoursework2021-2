import modules.FindTradeSuspect.MergeTables as fm
import pandas as pd 

def tradeVsPrice(date):
    data = fm.trade_and_dailyData(date)
    anomaly_trades = data.loc[(data['TradedSharePrice']>data['high'])|(data['TradedSharePrice']<data['low'])]
    anomaly_trades = pd.DataFrame(anomaly_trades).reset_index(drop=True)
    return anomaly_trades