import modules.DateConvertor.Convertor as dc
import modules.db.MongoDB.Query as mq 
import modules.db.SQL.Insert as si
import pandas as pd


def updatePortfolioPosition(date):
    conv_date = dc.dateConvertor(date)
    documents = mq.MongoDB_documents_btwDates(date)
    documents['Date'] = conv_date[1][0]
    updatePortfolio = documents.groupby(['Date', 'Trader', 'Symbol','Ccy']).agg( Quantity = ('Quantity', 'sum'), Notional = ('Notional','sum')).reset_index()
    updatePortfolio["pos_id"] = updatePortfolio["Trader"]+conv_date[1][1]+updatePortfolio["Symbol"]
    record_count = 0    
    for i in range(len(updatePortfolio)):
        try:
            new_entry = f'INSERT INTO portfolio_positions (pos_id, cob_date, trader, symbol, ccy, net_quantity, net_amount)\
                        VALUES ("{updatePortfolio.loc[i,"pos_id"]}",\
                                "{updatePortfolio.loc[i,"Date"]}",\
                                "{updatePortfolio.loc[i,"Trader"]}",\
                                "{updatePortfolio.loc[i,"Symbol"]}",\
                                "{updatePortfolio.loc[i,"Ccy"]}",\
                                 {updatePortfolio.loc[i,"Quantity"]},\
                                 {updatePortfolio.loc[i,"Notional"]})'
            si.SQL_insert(new_entry)
            record_count += 1
        except:
            print(f'Trade: {updatePortfolio.loc[i,"pos_id"]} already recorded')
    return record_count 