import pandas as pd
import modules.db.SQL.Connection as mc
import modules.DateConvertor.Convertor as dc

def SQL_insert(insert_entry):
    """insert_table: INSERT INTO new_file (id, name, email, joining_date, salary)
    VALUES
    (1,'James','james@pynative.com','2019-03-17',8000)"""

    con = mc.SQL_connection()
    cur = con.cursor()
    new_table = cur.execute(insert_entry)
    con.commit()
    con.close()
    return new_table