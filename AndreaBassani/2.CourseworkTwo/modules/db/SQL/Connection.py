import sqlite3
from sqlite3 import Error
import config.params as cp

def SQL_connection():
    """dbfile: r'H:\Git\iftcoursework2021\000.DataBases\SQL\Equity.db'"""
    #Create a SQL connection to our SQLite database 
    con = None 
    try:
        con = sqlite3.connect(cp.dbfile)
        return con
    except Error as e:
        print(e)
    return con