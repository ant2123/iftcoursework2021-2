import modules.UpdatePortfolioPosition.UpdateSQL as uu
import modules.FindTradeSuspect.UpdateSQL as fq
import config.params as cp
import test.UpdateTest as test


def decorator():
    record_total = 0
    suspect_total = 0
    for key in cp.days_to_check:
        record_count = uu.updatePortfolioPosition(cp.days_to_check[key])
        print(f'portfolio_positions updated, {record_count} new records on {cp.days_to_check[key]}')
        test.TestTradesNumber(cp.days_to_check[key])
        suspect_count = fq.updateTradeSuspect(cp.days_to_check[key])
        print(f'trade_subspect updated, {suspect_count} new suspect records')
    return print('Done')
    