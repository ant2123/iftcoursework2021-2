#%%
import datetime as dt
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from modules.db.MongoDB.MongoDBQuery import MongoDB_hystorical

dbfile =  r'H:\Git\iftcoursework2021\000.DataBases\SQL\Equity.db'
connection_path = 'mongodb://localhost'
database_name = 'Equity'
collection_name = 'CourseworkTwo'

def parameters (): 
    traderId = 'DMZ1796'
    enddate = dt.date(2021,11,12)
    startdate = min(MongoDB_hystorical(traderId)['DateTime'])
    return traderId, enddate, startdate

# %%
