from datetime import datetime 


def dateConvertor(date_original):
    '''converting the imputed date with format yyyy-mm-dd in a date that is digestible by the SQL and MongoDB queries'''
    date = datetime.strptime(date_original, '%Y-%m-%d')
    #ISODate first element on the array is the start the second the end
    ISODate = [f'ISODate({date.year}-{date.month:02d}-{date.day:02d}T00:00:00.000Z)',f'ISODate({date.year}-{date.month:02d}-{date.day:02d}T23:59:59.000Z)']
    #SQLDate first element is for query and the second is for the TradeId
    SQLDate = [f'{date.day}-{date.strftime("%b")}-{date.year}',f'{date.year}{date.month}{date.day}']
    return ISODate, SQLDate

def dateConvertor_SQL (date):
    #it converts 01-Jan-2021 in 2021-01-01
    return datetime.strptime(date, '%d-%b-%Y').strftime('%Y-%m-%d')

def dateConvertor_mongo (date):
    #it converts ISO... in 2021-01-01
    return datetime.strptime(date[8:18], '%Y-%m-%d').strftime('%Y-%m-%d')

