#%%
import configparser
import argparse
import sys
import datetime
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))


def get_config():
    parser = configparser.ConfigParser()
    parser.read("H:/3.CourseworkThree/config/script.config")
    sql_path = parser.get("config","sql_path")
    connection_path = parser.get("config","connection_path")
    database = parser.get("config","database")
    collection =parser.get("config","collection")
    enddate = parser.get("config","enddate")
    traderId = parser.get("config","traderId")
    return sql_path, connection_path, database, collection, enddate, traderId

def read_parse(args):
    parser = argparse.ArgumentParser(description="The parsing commands lists.")
    parser.add_argument("-d", "--date", help="Type the date in format 'YYYY-MM-DD'",type=str)
    opts = parser.parse_args(args)
    return opts




# %%
