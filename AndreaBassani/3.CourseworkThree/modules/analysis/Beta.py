#%%
import yfinance as yf
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
from analysis.ComulativeReturn import portfolio_vs_benchmark

def portfolio_beta ():
    table =  portfolio_vs_benchmark()
    x = np.array(table['Portfolio']).reshape((-1,1))
    y = np.array(table['SPY'])
    model = LinearRegression().fit(x, y)
    return model.coef_
# %%
