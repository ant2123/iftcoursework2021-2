    |_3.CourseworkThree
    |--config/
    |---script.config (.ini files are good too)
    |---script.params
    |--modules/
    |--static/
    |--src/
    |---App.* (if Shiny is used, then here should be placed server.R and ui.R)
    |--test/
    |--Home.html (mandatory for Use case 1 and 2, optional for the other)

