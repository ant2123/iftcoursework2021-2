#query1
db.CourseworkOne.find(
    {$and:[{"StaticData.GICSSector": "Health Care"},{"MarketData.Beta": {"$gte": 1}}]}).limit(3).pretty()

#query2
db.CourseworkOne.aggregate([
 {$match: {"StaticData.GICSSector": "Information Technology"} },
 {$group: {_id: "$StaticData.GICSSector", avg: {$avg: "$FinancialRatios.DividendYield"} } }])