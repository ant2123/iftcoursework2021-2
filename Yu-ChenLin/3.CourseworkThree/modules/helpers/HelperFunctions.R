printInfoLog <- function(message, type = "info"){
  
  type <- toupper(type)
  
  message <- paste0("[", type,"] ", Sys.time(), " ----- ", message, "\n \n")
  
  return(cat(message))
}