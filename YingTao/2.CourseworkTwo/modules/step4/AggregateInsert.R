#--------------------------------------------------------------------------------------
# UCL -- Institute of Finance & Technology
# Author  : Ying Tao
# Date: 2021-12-07
# Topic : Aggregate and Insert
#--------------------------------------------------------------------------------------
# SEE THE OTHER COMPLETE STEPS AND DATA IN THE FILE NAMED "MAIN.R"

#Step 4
#Aggregate Quantity and Notional for all trades by date, trader, symbol, ccy 

#firstly, we need to aggregate all the transaction on 2021-11-11 and 2021-11-12
#rename the format of the DateTime firstly
trades_sub$DateTime <-substring(trades_sub$DateTime,9,18)

#aggregate all the transaction on 2021-11-11 and 2021-11-12 by date, trader, symbol, ccy 
aggregate <- aggregate(cbind(Quantity, Notional)~DateTime+Trader+Symbol+Ccy, 
                       data = trades_sub, 
                       FUN = sum)


#secondly, aggregate the pos_id
#in order to generate unique IDs, we need idmaker, equals function(vec){return(paste(vec, collapse=""))}
#posid format combines Trader+ DateTime+ Symbol ----- connect them together to get the unique id
id = function(vec){return(paste(vec, collapse=""))}
posid <- apply(as.matrix(aggregate[,c("Trader", "DateTime", "Symbol")]), 1,id)
posid <- gsub("[[:punct:]]", "",posid)
aggregate <-cbind(posid,aggregate)

#thirdly, rename all the column
colnames(aggregate)
aggregate <- rename(aggregate,
                    c(pos_id = posid,
                      cob_date = DateTime,
                      trader = Trader,
                      symbol = Symbol,
                      ccy = Ccy,
                      net_quantity = Quantity,
                      net_amount = Notional))
colnames(aggregate)

#using dbAppendTable or dbWriteTable to append the aggregate we find in the table "portfolio_positions"
dbAppendTable(conn = conSql, name = "portfolio_positions", value = aggregate)
#totlly 3972 transactions in the table
#Alternately, using dbWriteTable(conn = conSql, name = "portfolio_positions", value = aggregate, append = TRUE)


#Finally, close the SQL and Mongo connections 
dbDisconnect(conSql)
conMongo$disconnect()
