
library(mongolite)
library(ggplot2)
library(dplyr)
library(lubridate)
library(RSQLite)

# Disable printing results in scientific notation
options(scipen=999)

# Importing data from SQL database
GITRepoDirectory <- "C:/Users/wilso/Desktop/CW3/iftcoursework2021"
setwd(GITRepoDirectory)
source(paste0(GITRepoDirectory, "./WeishengXu/3.CourseworkThree/config/script.config"))
source(paste0(GITRepoDirectory, "./WeishengXu/3.CourseworkThree/config/script.params"))
conSql <- dbConnect(RSQLite::SQLite(), paste0(GITRepoDirectory, Config$Directories$DBFolder,"/Equity.db"))
equityStatic <- RSQLite::dbGetQuery(conSql, "SELECT * FROM equity_static")
portfolioPosition <- RSQLite::dbGetQuery(conSql, "SELECT * FROM portfolio_positions")

# Opening connectivity to MongoDB database
conMongo <- mongo(collection = "CourseworkTwo", db = "Equity", url = "mongodb://localhost",
                  verbose = FALSE, options = ssl_options()) 

# Importing data from MongoDB
fulldata <- conMongo$find(query = "{}")
first_day <- conMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-11T00:00:00.000Z)\",
                            \"$lt\": \"ISODate(2021-11-12T00:00:00.000Z)\"}}")
second_day <- conMongo$find(query = "{\"DateTime\": {\"$gte\": \"ISODate(2021-11-12T00:00:00.000Z)\",
                            \"$lt\": \"ISODate(2021-11-13T00:00:00.000Z)\"}}")

# Setting a new column as Date in the first day and second day
first_day$Date <- "11-Nov-2021"
second_day$Date <- "12-Nov-2021"

# Aggregating the first day and the second day
aggregate11Q <- aggregate(Quantity~Date+Trader+Symbol+Ccy,first_day,sum)
aggregate11N <- aggregate(Notional~Date+Trader+Symbol+Ccy,first_day,sum)
aggregate11 <- left_join(aggregate11Q,aggregate11N)
aggregate12Q <- aggregate(Quantity~Date+Trader+Symbol+Ccy,second_day,sum)
aggregate12N <- aggregate(Notional~Date+Trader+Symbol+Ccy,second_day,sum)
aggregate12 <- left_join(aggregate12Q,aggregate12N)

# Set up pos_id
aggregate11$pos_id <- paste0(aggregate11$Trader,"20211111",aggregate11$Symbol,sep="")
aggregate12$pos_id <- paste0(aggregate12$Trader,"20211112",aggregate12$Symbol,sep="")

# Selecting the column we need to insert into the SQL Database
result11 <- select(aggregate11,pos_id,Date,Trader,Symbol,Ccy,Quantity,Notional)
result12 <- select(aggregate12,pos_id,Date,Trader,Symbol,Ccy,Quantity,Notional)

# Combining both results into one table and naming it as totalresult 
totalresult <- rbind(result11,result12)

# Renaming the column names to be same as the names in SQL database
colnames(totalresult) <- c("pos_id","cob_date", "trader","symbol","ccy","net_quantity","net_amount") 

# Combining the portfolio position table with the results above
NewPortfolioPosition <- rbind(portfolioPosition,totalresult)

# Merging data from new portfolio_position table and equity_static table
MergeResult <- merge(NewPortfolioPosition, equityStatic, by= 'symbol')

# Selecting the columns and aggregating the net amount which based on cob_date, trader and GISSector and another net amount which based on cob_date and trader
result3 <- select(MergeResult,cob_date,trader,net_amount,GICSSector)
NetamountTable <- aggregate(net_amount~cob_date+trader+GICSSector,result3,sum)
NetamountTable2 <- aggregate(net_amount~cob_date+trader,NetamountTable,sum)

# Renaming the column to TotalAmount in order to get better visualisation
colnames(NetamountTable2)[colnames(NetamountTable2) == 'net_amount'] <- 'TotalAmount'

# Merging the two tables based on the columns of cob_date and trader, then calculating the exposures by the formula
Portfolio <- merge(NetamountTable, NetamountTable2, by=c('cob_date','trader'))
Portfolio$Exposure <- Portfolio$net_amount/Portfolio$TotalAmount


