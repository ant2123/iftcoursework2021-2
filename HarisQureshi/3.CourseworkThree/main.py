# entrance API
#%%
from src.createHTML import createHTML
from modules.component.graph import plot_pie, plot_line, plot_table_1, plot_table_2

if __name__ == '__main__':
    plot_pie() # step1: create figures
    plot_line()
    plot_table_1()
    plot_table_2()
    createHTML() # step2: create HTML
    print('Finished')

# %%