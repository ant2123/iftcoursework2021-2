#%% setting up this file and importing all the packages/params/data

# importing packages and setting up path
from os.path import dirname
import os
import plotly.express as px
import sys 
import pandas as pd 
from datetime import datetime as dt
import numpy as np
sys.path.append( 
     dirname( dirname( dirname(__file__) ) )
 )

# getting the params and data
from config.params import Trader,Date 
from modules.input.data import readAndProcessData 
import matplotlib.pyplot as plt
import plotly.graph_objects as go

#%% ploting the graphs and table using the data from modules.input.data


def plot_line():
        # read data 
        NewPortfolioPosition = readAndProcessData(Trader,Date)
        # change the data (df)
        df_line = NewPortfolioPosition.groupby(['Date'])['ReturnEquity'].apply(np.nansum).reset_index()
        df_line = df_line.drop(df_line.index[df_line['Date'] == '2020-01-06'])
        # graph configuration
        fig = px.line(df_line, x="Date", y="ReturnEquity", title= 'Portfolio Return for Trader JBX1566 between 15 Oct 2021 - Nov 12 2021')
        # export fig to picture 
        fig.write_image(os.path.join(dirname(dirname(dirname(__file__))),r'src\line_chart.png'))


def plot_pie():
        # read data
        NewPortfolioPosition = readAndProcessData(Trader,Date)
        df_pie = NewPortfolioPosition.groupby(['GICSSector'])['net_amount'].apply(np.nansum)
        df_pie = pd.DataFrame(df_pie).reset_index()
        df_pie['percentage'] = (df_pie['net_amount']/df_pie['net_amount'].sum()).values
        # graph configuration
        labels = df_pie['GICSSector'].tolist()
        values = df_pie['percentage'].tolist()
        pulls = (1*(df_pie['percentage'] == df_pie['percentage'].max())).values
        # pull is given as a fraction of the pie radius to the piece highlighted
        fig = go.Figure(data=[go.Pie(labels=labels, values=values, pull=0.2 * pulls)])
        # export fig to picture
        fig.write_image(os.path.join(dirname(dirname(dirname(__file__))),r'src\pie_chart.png'))

#def plot_bar():
        #read
 #       df = px.data.gapminder().query("continent == 'Europe' and year == 2007 and pop > 2.e6")
  #      fig = px.bar(df, y='pop', x='country', text_auto='.2s',
   #         title="Controlled text sizes, positions and angles")
    #    fig.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
     #   fig.show()

def plot_table_1():
        # import data
        NewPortfolioPosition = readAndProcessData(Trader,Date)
        # portfolio return 
        PortfolioReturn = NewPortfolioPosition['ReturnEquity'].sum()
        # top 5 sector returns
        TopSectorReturn = NewPortfolioPosition.groupby(['GICSSector'])['ReturnEquity'].apply(np.nansum).sort_values()
        TopSectorReturn = TopSectorReturn.sort_values(ascending=False).head(2).reset_index()
        # top 5 industry returns
        TopIndustryReturn = NewPortfolioPosition.groupby(['GICSIndustry'])['ReturnEquity'].apply(np.nansum).sort_values()
        TopIndustryReturn = TopIndustryReturn.sort_values(ascending=False).head(2).reset_index()
        # top 5 stock returns
        TopStockReturn = NewPortfolioPosition.groupby(['symbol'])['ReturnEquity'].apply(np.nansum).sort_values()
        TopStockReturn = TopStockReturn.sort_values(ascending=False).head(2).reset_index()
        
        # create table
        values=[['Portfolio', 'Sector 1', 'Sector 2', 'Industry 1', 'Industry 2', 'Stock 1', 'Stock 1'],
               ['Overall Position', TopSectorReturn.loc[0, 'GICSSector'], TopSectorReturn.loc[1, 'GICSSector'], TopIndustryReturn.loc[0, 'GICSIndustry'], TopIndustryReturn.loc[1, 'GICSIndustry'], TopStockReturn.loc[0, 'symbol'], TopStockReturn.loc[1, 'symbol']],
               [PortfolioReturn, TopSectorReturn.loc[0, 'ReturnEquity'], TopSectorReturn.loc[1, 'ReturnEquity'], TopIndustryReturn.loc[0, 'ReturnEquity'], TopIndustryReturn.loc[1, 'ReturnEquity'], TopStockReturn.loc[0, 'ReturnEquity'], TopStockReturn.loc[1, 'ReturnEquity']]]
        # graph configuration
        fig = go.Figure(data=[
                        go.Table(
                                columnorder = [1,2,3],
                                columnwidth = [200, 400, 400],
                                header = dict(
                                values = [['<b>Category</b>'],
                                        ['<b>Name</b>'],
                                        ['<b>Return</b>']],
                                line_color='darkslategray',
                                fill_color='royalblue',
                                align=['center','center', 'center'],
                                font=dict(color='white', size=12),
                                height=40
                         ),
                        cells=dict(
                                values=values,
                                line_color='darkslategray',
                                fill=dict(color=['paleturquoise', 'white']),
                                align=['left', 'center'],
                                font_size=12,
                                height=30)
                        )
                ])
        fig.write_image(os.path.join(dirname(dirname(dirname(__file__))),r'src\table_1.png'))


def plot_table_2():
        # import data
        NewPortfolioPosition = readAndProcessData(Trader,Date)
        # standard deviation for retuns
        StandardDeviation = pd.DataFrame(NewPortfolioPosition['ReturnEquity']).std()
        # other portfolio descriptive metrics
        SectorNumber = NewPortfolioPosition.groupby(['GICSSector'])['net_amount'].apply(np.nansum).info
        IndustryNumber = NewPortfolioPosition.groupby(['GICSIndustry'])['net_amount'].apply(np.nansum).info
        MoneyInvested = NewPortfolioPosition['net_amount'].sum()
        # create table
        values=[['Standard Deviation','Sectors Invested','Industries Invested', 'Amount Invested'],
                [StandardDeviation,'11','122', MoneyInvested]]
        # graph configuration
        fig = go.Figure(data=[
                        go.Table(
                                columnorder = [1,2],
                                columnwidth = [200,400],
                                header = dict(
                                values = [['<b>Metric</b>'],
                                        ['<b>Result</b>']],
                                line_color='darkslategray',
                                fill_color='royalblue',
                                align=['left','center'],
                                font=dict(color='white', size=12),
                                height=40
                         ),
                        cells=dict(
                                values=values,
                                line_color='darkslategray',
                                fill=dict(color=['paleturquoise', 'white']),
                                align=['left', 'center'],
                                font_size=12,
                                height=30)
                        )
                ])
        fig.write_image(os.path.join(dirname(dirname(dirname(__file__))),r'src\table_2.png'))

        
#%%


