import pymongo

def outputToMongoDB(trade):
    my_client = pymongo.MongoClient("mongodb://localhost:27017/")
    trade_DB = my_client['equity']
    trade_DB = trade_DB["CourseworkTwo"]

    #["DateTime", "TradeId", "Trader", "Symbol", "Quantity", "Notional", "TradeType", "Ccy", "Counterparty"]

    dict={"DateTime":trade.DateTime,"TradeId":trade.TradeId,"Trader":trade.Trader,"Symbol":trade.Symbol,"Quantity":trade.Quantity,"Notional":trade.Notional,"TradeType":trade.TradeType, "Ccy":trade.Ccy, "Counterparty":trade.Counterparty}

    trade_DB.insert_one(dict)
