import sqlite3
import dateutil.parser
from fastapi import FastAPI
import uvicorn
import re

connect = sqlite3.connect("Equity.db")
cursor = connect.cursor()

creat_table_sql = """create table trades_suspects
(
    DateTime TEXT,
    TradeId TEXT,
    Trader TEXT,
    Symbol TEXT,
    Quantity FLOAT,
    Notional FLOAT ,
    TradeType TEXT,
    Ccy TEXT,
    Counterparty TEXT

);

"""

try:
    cursor.execute(creat_table_sql)
except:
    ...

creat_table_sql = """create table error_data
(
    DateTime TEXT,
    TradeId TEXT,
    Trader TEXT,
    Symbol TEXT,
    Quantity FLOAT,
    Notional FLOAT ,
    TradeType TEXT,
    Ccy TEXT,
    Counterparty TEXT
);

"""
try:
    cursor.execute(creat_table_sql)
except:
    ...
creat_table_sql = """create table normal_equity
(
    DateTime TEXT,
    TradeId TEXT,
    Trader TEXT,
    Symbol TEXT,
    Quantity FLOAT,
    Notional FLOAT ,
    TradeType TEXT,
    Ccy TEXT,
    Counterparty TEXT
);

"""
try:
    cursor.execute(creat_table_sql)
except:
    ...

connect.commit()
connect.close()

query_sql = """select * from equity_prices where cob_date='%s' and symbol_id='%s'"""

insert_sql = """insert into trades_suspects values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"""
insert_error_sql = """insert into error_data values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"""
insert_normal_sql = """insert into normal_equity values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"""


def get_config():
    conf.read("./config/script.config")
    x = conf.get("config","Database")
    SQLname = x
    x = conf.get("config","dbconnection")
    #print(x)
    sql_conn = importlib.import_module(x).sql_conn
    return SQLname, sql_conn

def get_params():
    conf.read("./config/script.params")
    x = conf.get("params","db")
    db = x
    x = conf.get("params","collection")
    collection = x
    return db, collection
app = FastAPI()


@app.get('/submit')
def submit(DateTime: str, TradeId: str, Trader: str,
           Symbol: str, Quantity: float, Notional: float,
           TradeType: str, Ccy: str, Counterparty: str):
           
    SQLname, sql_conn= get_config()
    db, collection= get_params()
    
    conn, cur = con_db(SQLname)


    for _ in [0]:
        l = tuple([DateTime, TradeId, Trader, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty])
        if DateTime and TradeId and Trader and Symbol and Quantity and Notional and TradeType and Ccy and Counterparty:
            try:
                newDateTime = re.findall(r'ISODate\((.{24})\)', DateTime)[0]
                newDateTime = dateutil.parser.parse(newDateTime).strftime("%d-%b-%Y")
            except:
                print("incorrect DateTime")
                print('insert error data')
                cur.execute(insert_error_sql % l)
                break

            sql_format = query_sql % (newDateTime, Symbol)
            cur.execute(sql_format)
            fetchone = cur.fetchone()
            if fetchone:
                print(fetchone)
                price = Notional / Quantity
                if not fetchone[3] < price < fetchone[2]:
                    print("insert suspect data")
                    cur.execute(insert_sql % l)
                else:
                    print("insert normal data")

                    cur.execute(insert_normal_sql % l)
            else:
                print('insert error data')
                cur.execute(insert_error_sql % l)

        else:
            print('insert error data')
            cur.execute(insert_error_sql % l)
    connect.commit()
    connect.close()
    return {'success': True}


uvicorn.run(app, host="0.0.0.0", port=8000)
