import json
import sqlite3

#load json file
json_data = json.load(open('CourseworkTwo.json', 'r', encoding='utf8'))

#connect Equity.db
connect = sqlite3.connect("Equity.db")
cursor = connect.cursor()

#commit db
connect.commit()

#close db
connect.close()
