import json
import sqlite3
import pandas as pd

json_data = json.load(open('CourseworkTwo.json', 'r', encoding='utf8'))
json_data = pd.DataFrame(json_data)

day_two_data = []

for row in json_data.iterrows():
    row = row[1]
    if '11-11' in row.DateTime:
        row.DateTime = '11'
        day_two_data.append(row)
    if '11-12' in row.DateTime:
        row.DateTime = '12'
        day_two_data.append(row)

day_two_data = pd.DataFrame(day_two_data)
day_two_data = day_two_data.reset_index(drop=True)
day_two_data['price'] = day_two_data['Notional'] / day_two_data['Quantity']

connect = sqlite3.connect("Equity.db")
cursor = connect.cursor()
creat_table_sql = """create table trades_suspects
(
    price_id  TEXT
        primary key,
    open      INTEGER,
    high      INTEGER,
    low       INTEGER,
    close     INTEGER not null,
    volume    INTEGER not null,
    currency  TEXT    not null,
    cob_date  TEXT    not null,
    symbol_id TEXT    not null
        references equity_static
);

"""
try:
    cursor.execute(creat_table_sql)
except:
    ...

query_sql = """select * from equity_prices where cob_date='%s-Nov-2021' and symbol_id='%s'"""
insert_sql = """insert into trades_suspects values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"""

for row in day_two_data.iterrows():
    row = row[1]
    sql_format = query_sql % (row.DateTime, row.Symbol)

    cursor.execute(sql_format)
    fetchone = cursor.fetchone()

    if not fetchone[3] < row.price < fetchone[2]:
        cursor.execute(insert_sql % fetchone)
        print(fetchone)

connect.commit()
connect.close()