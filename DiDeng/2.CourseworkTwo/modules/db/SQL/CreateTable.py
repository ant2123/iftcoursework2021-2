# Use SQL query to create a table with primary key and foreign key and load data into it.
conn = sqlite3.connect("C:/Users/dell/Desktop/Big Data/cw/iftcoursework2021/000.DataBases/SQL/Equity.db")
cur=conn.cursor()
def SQLCreateTable():
    Command=cur.execute('''CREATE TABLE trades_suspects(\
    DateTime TEXT PRIMARY KEY NOT NULL,\
    TradeId TEXT,\
    Trader TEXT,\
    Symbol TEXT,\
    Quantity INTEGER,\
    Notional FLOAT,\
    TradeType TEXT,\
    Ccy TEXT,\
    Counterparty TEXT,\
    FOREIGN KEY ("Symbol") REFERENCES "price_id"("symbol_id")
    )''')