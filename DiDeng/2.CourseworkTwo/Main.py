#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import packages
import sqlite3
import json
from scipy import stats
import numpy as np
from sqlalchemy import create_engine
import datetime
import matplotlib.pyplot as plt
from pymongo import MongoClient
import pandas as pd

# connect mongo and read json
mongocon=MongoClient('mongodb://localhost')
db=mongocon.equity
col=db.coursework2
trades = col.find({})
trades = pd.DataFrame(trades)

# connect sql and read database
Directory = "C:/Users/dell/Desktop/Big Data/cw"
engine = create_engine(f"sqlite:///{Directory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()
def SQLQueryStatic():
    return ("SELECT * FROM equity_prices")
equitystatic = pd.read_sql_query(SQLQueryStatic(),engine)


# In[2]:


# second way of connect
#read json:
#trades = pd.read_json('C:/Users/dell/Desktop/Big Data/cw/iftcoursework2021/000.DataBases/NoSQL/CourseworkTwo.json')

#read sql
#conn = sqlite3.connect("C:/Users/dell/Desktop/Big Data/cw/iftcoursework2021/000.DataBases/SQL/Equity.db")  #dir_in+filename
#cur = conn.cursor()


# In[3]:


# check the structure of data
print(trades.keys(),equitystatic.keys())


# In[4]:


# print yy_mm_dd of json trader file:
def isodate_to_yymmdd(isodate):
    year_json=isodate[8:12]
    month_json=isodate[13:15]
    day_json=isodate[16:18]
    return year_json,month_json,day_json
year_json=np.zeros(len(trades["DateTime"])).astype(int)
month_json=np.zeros(len(trades["DateTime"])).astype(int)
day_json=np.zeros(len(trades["DateTime"])).astype(int)
for i in range(len(trades["DateTime"])):
    year_i,month_i,day_i=isodate_to_yymmdd(trades["DateTime"][i])
    year_json[i]=int(year_i)
    month_json[i]=int(month_i)
    day_json[i]=int(day_i)
date=year_json*10000+month_json*100+day_json
tradesid=np.zeros(len(trades["DateTime"])).astype(str)
for i in range(len(trades["DateTime"])):
    tradesid[i]=str(date[i])+str(trades["Symbol"][i])

equityid=np.array(equitystatic["price_id"]).astype(str)
print(date)


# In[5]:


# Find the location of unique trades record
uniqueid=np.unique(tradesid)
index_json=[[] for i in range(len(uniqueid))]
index_db=[[] for i in range(len(uniqueid))] 
for i in range(len(uniqueid)):
    index_json_i=np.where(tradesid==uniqueid[i])[0]
    index_json[i]=index_json_i
    index_db_i=np.where(equityid==uniqueid[i])[0]
    index_db[i]=index_db_i
# print(index_json, index_db)
print(uniqueid,tradesid)


# In[6]:


# organize data
trades_quantity=trades["Quantity"].to_numpy()
trades_notional=trades["Notional"].to_numpy()
equitystatic_volume=equitystatic["volume"].to_numpy()
equitystatic_high=equitystatic["high"].to_numpy()
equitystatic_low=equitystatic["low"].to_numpy()
equitystatic_open=equitystatic["open"].to_numpy()
equitystatic_close=equitystatic["close"].to_numpy()

# Test the first uniqueid
#print(index_json[3])
#print(index_db[3])
#print(trades_quantity[index_json[3]])
#print(trades_notional[index_json[3]])
#print(equitystatic_volume[index_db[3]])
#print(equitystatic_open[index_db[3]])
#print(equitystatic_close[index_db[3]])
#print(equitystatic_high[index_db[3]])
#print(equitystatic_low[index_db[3]])
diff=(equitystatic_high[index_db[3]]-equitystatic_low[index_db[3]])/equitystatic_open[index_db[3]]
#print(diff)
price=trades_notional[index_json[3]]/trades_quantity[index_json[3]]-equitystatic_open[index_db[3]]     
#print(price)


# In[7]:


# trade vs price
for i in range(len(uniqueid)):
    avg=trades_notional[index_json[i]]/trades_quantity[index_json[i]]
    for j in range(len(avg)):
        if avg[j] > equitystatic_high[index_db[i]][0] or avg[j] < equitystatic_low[index_db[i]][0]:      
            print(tradesid[index_json[i][j]],i,j)


# In[8]:


# check and find the trades of outliers
print(index_json[3128],index_json[3153][0],index_json[3348],index_json[3361])     
print(trades.iloc[4065])
print(trades.iloc[4047])
print(trades.iloc[4369])
print(trades.iloc[4355])


# In[9]:


# Use SQL query to create a table with primary key and foreign key and load data into it.

#conn = sqlite3.connect("C:/Users/dell/Desktop/Big Data/cw/iftcoursework2021/000.DataBases/SQL/Equity.db")
#cur=conn.cursor()
#def SQLCreateTable():
#    Command=cur.execute('''CREATE TABLE trades_suspectsssss(\
#    DateTime TEXT PRIMARY KEY NOT NULL,\
#    TradeId TEXT,\
#    Trader TEXT,\
#    Symbol TEXT,\
#    Quantity INTEGER,\
#    Notional FLOAT,\
#    TradeType TEXT,\
#    Ccy TEXT,\
#    Counterparty TEXT,\
#    FOREIGN KEY ("Symbol") REFERENCES "price_id"("symbol_id")
#    )''')

#LoadData="INSERT INTO trades_suspectsssss (DateTime, TradeId, Trader, Symbol, Quantity, Notional, TradeType, Ccy, Counterparty)\
#        VALUES ("
#for k in range(len(index_json_select)):
#    LoadData=LoadData+"'"+str(trades["DateTime"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["TradeId"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["Trader"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["Symbol"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["Quantity"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["Notional"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["TradeType"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["Ccy"][index_json_select[k]])+"'"
#    LoadData=LoadData+"'"+str(trades["Counterparty"][index_json_select[k]])+"'"
#return(Command)
#conn.execute(LoadData)
#conn.commit()
#print(cur.fetchall())             


# In[10]:


# Observe data 
index_db2=np.array(index_db).reshape(len(uniqueid),)
print(index_db2)
diff=(equitystatic_high[index_db2]-equitystatic_low[index_db2])/equitystatic_open[index_db2]
print(diff)
plt.hist(diff)
plt.show()


# In[11]:


plt.scatter(equitystatic_high[index_db2],equitystatic_low[index_db2],s=5)
plt.show()


# In[12]:


# Reshape to match each trades record with the market data in the same day and same stock
index_json3=np.array([])  
index_db3=np.array([])
for i in range(len(uniqueid)):
    index_json3=np.concatenate((index_json3,index_json[i]))
    index_db3=np.concatenate((index_db3,index_db[i][0]*np.ones(len(index_json[i]))))
index_json3=index_json3.astype(int)
index_db3=index_db3.astype(int)
print(index_json3)
print(index_db3)


# In[13]:


# Inconsistency between quantity traded and nitonal amounts
diff_price=np.zeros(len(index_json3))
for i in range(len(index_json3)):
    diff_price[i]=(trades_notional[index_json3[i]]/trades_quantity[index_json3[i]]-equitystatic_open[index_db3[i]])/equitystatic_open[index_db3[i]]
date3=date[index_json3]
print("date3",date3)
print(diff_price)
print(np.percentile(diff_price,10),np.percentile(diff_price,50),np.percentile(diff_price,90))


# In[14]:


# change to log to observe data clearly
plt.hist(diff_price)
plt.xscale("log")
plt.yscale("log")      
plt.show()
print(max(diff_price),min(diff_price))


# In[15]:


# Hypothesis testing - three sigma limits
loc=np.median(diff_price)
scale=np.zeros(100)
for i in range(100):
    index_random=np.random.choice(len(diff_price),int(0.1*len(diff_price)),replace=False)
    scale[i]=np.std(diff_price[index_random],ddof=1)
print("bootstrap",scale)
print(len(diff_price))
scale=np.median(scale)
print("loc,scale",loc,scale)
print(loc-3*scale,loc+3*scale)
index_low=np.where((diff_price<loc-3*scale)&(date3>=20211111)&(date3<=20211112))[0]
index_up=np.where((diff_price>loc+3*scale)&(date3>=20211111)&(date3<=20211112))[0]
index_json_select=np.concatenate([index_json3[index_low],index_json3[index_up]])
print("index_json_select",index_json_select)
index_db_select=np.concatenate([index_db3[index_low],index_db3[index_up]])
print("index_db_select",index_db_select)
print(trades["DateTime"][index_json_select])
trades_suspects=trades.loc[index_json_select,['DateTime', 'TradeId', 'Trader', 'Symbol', 'Quantity',  'Notional', 'TradeType', 'Ccy', 'Counterparty']]
trades_suspects["Notional"]=trades_suspects["Notional"].astype(str)
print(trades_suspects.keys())
#'DateTime', 'TradeId', 'Trader', 'Symbol', 'Quantity',  'Notional', 'TradeType', 'Ccy', 'Counterparty']
print(type(trades_suspects))
trades_suspects.to_sql("trades_suspects",con,if_exists="replace",index=False)
x=np.linspace(np.percentile(diff_price,5),np.percentile(diff_price,95),1000)
pdf=stats.norm.pdf(x, loc=loc, scale=scale)
plt.plot(x,pdf)
plt.show()


# In[16]:


# Aggregate data and insert suspect trades into sql
select=np.where((date<=20211112)& (date>=20211111))[0]
trades_select=trades.loc[select,:]
pos_id=np.zeros(len(trades_select)).astype(str)
for i in range(len(trades_select)):
    pos_id[i]=trades_select["Trader"][select[i]]+str(date[select][i])+trades_select["Symbol"][select[i]]
#print(pos_id)
unique_pos_id=np.unique(pos_id)
agg_notional=np.zeros(len(unique_pos_id))
agg_quantity=np.zeros(len(unique_pos_id))
agg_trader=np.zeros(len(unique_pos_id)).astype(str)
agg_symbol=np.zeros(len(unique_pos_id)).astype(str)
agg_cob_date=np.zeros(len(unique_pos_id)).astype(str)
agg_ccy=np.zeros(len(unique_pos_id)).astype(str)
for i in range(len(unique_pos_id)):
    index=np.where(pos_id==unique_pos_id[i])[0]
    agg_notional[i]=np.sum(trades_select["Notional"][select[index]])
    agg_quantity[i]=np.sum(trades_select["Quantity"][select[index]])
#    print(trades["Trader"][index[0]])
    agg_trader[i]=trades_select["Trader"][select[index[0]]]
    agg_symbol[i]=trades_select["Symbol"][select[index[0]]]
    agg_cob_date[i]=trades_select["DateTime"][select[index[0]]]
    agg_ccy[i]=trades_select["Ccy"][select[index[0]]]
def isodate_to_cob_date(agg_cob_date):
    month_symbol=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    for i in range(len(agg_cob_date)):
        year=agg_cob_date[i][8:12]
        month=month_symbol[int(agg_cob_date[i][13:15])-1]
        day=agg_cob_date[i][16:18]
        agg_cob_date[i]=day+"-"+month+"-"+year
isodate_to_cob_date(agg_cob_date)
table_agg=pd.DataFrame({"pos_id":unique_pos_id,
                       "cob_date":agg_cob_date,
                       "trader":agg_trader,
                       "symbol":agg_symbol,
                       "ccy":agg_ccy,
                       "net_quantity":agg_quantity.astype(int),
                       "net_amount":agg_notional.astype(int)})
table_agg.to_sql("portfolio_positions",con,if_exists="append",index=False)
print(table_agg)


# In[17]:


# close connections
con.close()
mongocon.close()

