from django.shortcuts import render

from backend.calculations import calculations
from backend.App import App
import json
# Create your views here.
def Home_page(request,*args,**kwargs):

    trader_name,trader_id,net_trade,prof,expense,buy_count,sell_count,join_date,sell_qty,buy_qty = calculations()
    context = {'traders': trader_name, 'ids': trader_id[0],'net':net_trade,'range':[0,1],'profit':prof,'exp':expense,'buy_count':buy_count,'sell_count':sell_count,
    'joined':join_date[0],
    'sell_qty':sell_qty,
    'buy_qty': buy_qty
    }

    return render(request,"index.html",context={'data':context})

def Q2(request,*args,**kwargs):
    name, id,date,sell_qty,buy_qty,risk = App()
   
    context = {'traders': name, 'ids': id,'range':[0,1],'risk':risk,
    'joined':date,
    'sell_qty':sell_qty,
    'buy_qty': buy_qty
    }

    return render(request,"Home.html",context={'data':context})