from ast import JoinedStr
from backend.ApplyQuery import *
from backend.Databases import *
# from ApplyQuery import *
# from Databases import *
import json

def traders_info():
    # Importing Trader Data
    data = Query(trader_statics,col=['trader_id',"trader_name","golive_date"],query=SELECT)
    trader_name = []
    trader_id = []
    date = []
    for row in data:
        trader_id.append(row[0])
        trader_name.append(row[1])
        date.append(row[2])
    return trader_name,trader_id,date

def App():
    df = open("C:\Users\arya8\OneDrive\Desktop\CourseworkTwo.json")
    df1 = open("C:\Users\arya8\OneDrive\Desktop\CourseworkOne.json")
    file = json.load(df)
    df1 = json.load(df1)
    trader_name,trader_id,join_date = traders_info()
#Data
    count = 0
    expense = 0.0
    earning = 0.0
    sell_qty = 0
    buy_qty = 0
#Buying and Selling Calculations:   
    for lines in file:
        if str(lines['Trader']) == trader_id[2]:
            date = (str(lines['DateTime']).split("(")[1]).split("T")[0]
            if date == "2021-11-12":
                qty = float(lines['Quantity'])
                item_symbol = lines['Symbol']
                type_name = lines['TradeType']
                for symbol_file in df1:
                    if item_symbol == symbol_file['Symbol']:
                        rate = symbol_file['MarketData']['Price']
                        if type(rate) == str:
                            rate = 0.0
                        if type_name == "BUY":
                            expense = (float(rate) * qty) + expense
                            buy_qty += lines['Quantity']
                        else:
                            qty = qty * (-1)
                            earning = (rate * qty) + earning
                            sell_qty += lines['Quantity']
                            sell_qty = sell_qty * (-1)
                        break
            count += 1
#Risk Calculation:            
    risk = (expense * earning)/100
    return [trader_name[2]],trader_id[2],earning,expense,join_date[2],sell_qty,buy_qty,risk



