//-----------------------------------------------------------------------------------------------------------------------------------------------------------------
// Big Data in Quantitative Finance
// Author  : Farid Rajabov
// Topic   : Coursework 1 - NoSQL
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------


//----NoSQL Query 1 - WE FIND AVERAGE PERatio----------------------------------------------------------------------------------------------------------------

db.CourseworkOne.aggregate([

{$match:{'StaticData.GICSSector':'Health Care'}},

{$group:{_id:'$StaticData.GICSSector', Avg_PERatio:{$avg:'$FinancialRatios.PERatio'}}}

]).pretty()



//----NoSQL Query 2 - //WE FIND COMPANIES SUITABLE TO OUR CONDITIONS----------------------------------------------------------------------------------------

db.CourseworkOne.find({$and:[{'StaticData.GICSSector': "Health Care"},
{'MarketData.Beta':{"$lt": 1}},{'FinancialRatios.PERatio':{"$lt": 26}},{'FinancialRatios.PERatio':{"$ne": 0}}]},

{'StaticData.Security': 1,'MarketData.Beta': 1,'FinancialRatios.PERatio': 1}
).sort({'MarketData.Beta': -1}).pretty()



//////======================================




