"""
@author: Jean Ali Useni Kikaya
"""
#%%
from enum import auto
from fileinput import close
from gettext import install
from operator import index
from posixpath import dirname
from symtable import Symbol
from time import strftime
from tkinter import font
from h11 import InformationalResponse
import pymongo
from pymongo import MongoClient
import pandas as pd
import numpy as np
from sqlalchemy import column, create_engine
import matplotlib.pyplot as plt
from datetime import datetime as dt
import seaborn as sb
from jinja2 import Environment, FileSystemLoader
import pandas_datareader as web
from scipy import stats
import sys
sys.path.append("..")
import os
import configparser
from modules.input.SQLQueries import SQLQueryClosePrices,SQLQueryStatic
from modules.db.db_connection import SQLite, MongoDB
#%% Paths for HTML and config
config_path = os.path.realpath('..')
FileName = 'config\script.ini'
completeName = os.path.join(config_path, FileName)
save_path = os.path.realpath('..')
file = completeName
#%% agruments to pass on command line
TraderID = sys.argv[1]
start_date = sys.argv[2]
end_date = sys.argv[3]
#%%
print('Portfolio Metrics Analysis for',end_date)
#%% Connection to databases
engine = SQLite()
collection = MongoDB()
#%% SQLite and MongoDB queries
Mongo_Trades = collection.find()
Equity = pd.read_sql_query(SQLQueryStatic(),engine)
SQL_Prices = pd.read_sql_query(SQLQueryClosePrices(),engine)

#%% Creating Tables
EquityStatic = pd.DataFrame(Equity)
Prices = pd.DataFrame(SQL_Prices)
Mongo_Trades_df = pd.DataFrame(Mongo_Trades)
EquityStatic
#%% Cleaning
Mongo_Trades_df["Date"] = Mongo_Trades_df.DateTime.apply(lambda x: dt.strptime(x, "ISODate(%Y-%m-%dT%H:%M:%S.000Z)") ).values
Mongo_Trades_df["Date"] = Mongo_Trades_df.Date.apply(lambda x: dt.strftime(x, '%Y-%m-%d') ).values
Mongo_Trades_df["Date"] = Mongo_Trades_df["Date"].apply(pd.to_datetime)
Prices["Date"] = SQL_Prices.cob_date.apply(lambda x: dt.strptime(x, "%d-%b-%Y")).values
Prices = Prices.drop(columns=['cob_date'])
Mongo_Trades_df.sort_values('Date')
PivotPrices = Prices.pivot(index="Date", columns="Symbol", values="close")

#%% Sector Allocation
TradeTable = pd.merge(EquityStatic,Mongo_Trades_df,on='Symbol')
TradeTable = TradeTable[TradeTable['Date']==end_date]
SectorTableOne = TradeTable.groupby(['Trader','GICSSector']).agg(Quantity=('Quantity','sum'),Notional=('Notional','sum')).reset_index()
SectorTableTwo = SectorTableOne.set_index('GICSSector')
TraderSector = SectorTableTwo[SectorTableTwo['Trader']==TraderID]
TraderSector.plot.pie(y='Notional', figsize=(90, 75),fontsize=80,autopct='%1.1f%%')
plt.legend(loc='upper center', bbox_to_anchor=(1,0.25), fontsize=80)
plt.savefig(save_path+'\SectorBreakdown.png')
plt.close()

#%% SPY Benchmark
SPYIndex = web.DataReader('SPY',data_source='yahoo',start= start_date,end=end_date)
SPYIndex = pd.DataFrame(SPYIndex)
SPYIndex = SPYIndex.drop(columns=['High','Low','Open','Volume','Adj Close'])
SPYIndex = SPYIndex.rename(columns={"Close":"SPY"})
SPYIndexChange = SPYIndex.pct_change()[1:]
SPYIndexReturn = np.log(1+SPYIndexChange)
NewPrices = pd.merge(SPYIndex,PivotPrices,on='Date')
#%%
USBondETF = web.DataReader('GOVT',data_source='yahoo',start= start_date,end=end_date)
USBondETF = pd.DataFrame(USBondETF)
USBondETF = USBondETF.drop(columns=['High','Low','Open','Volume','Adj Close'])
USBondETF = USBondETF.rename(columns={"Close":"GOVT"})
USBondETFChange = USBondETF.pct_change()
USBondETFReturn = np.log(1+USBondETFChange)
USBondETFMean = pd.DataFrame(USBondETFReturn.mean())
USBondETFMean.columns = ['Average Return']

#%% Calculating individual Stock Returns 
NewPricesChange = NewPrices.pct_change()
NewPricesReturn = np.log(1+NewPricesChange)
ReturnMean = pd.DataFrame(NewPricesReturn.mean())
ReturnMean.columns = ['Average Return']

#%% Trader Selection
Mongo_positions = Mongo_Trades_df.groupby(['Symbol','Date', 'Trader']).agg(Quantity=('Quantity','sum'),Notional=('Notional','sum')).reset_index()
SpecificTrader = Mongo_positions[Mongo_positions['Trader']==TraderID]
Date = (SpecificTrader['Date']==end_date)
SpecificTrader = SpecificTrader.loc[Date]
SpecificTrader.groupby('Symbol').agg(Quantity=('Quantity','sum'),Notional=('Notional','sum')).reset_index()
#%% Creating a list of stock in the trader's portfolio
StockList = SpecificTrader['Symbol'].tolist()
list = Prices.Symbol.isin(StockList)
PortfolioStocks = Prices[list]
#%% Average Return for  individual stocks in the portfolio
PortClosePrice = PortfolioStocks.pivot(index="Date", columns="Symbol", values="close")
PortRet = np.log(1+PortClosePrice.pct_change())
MeanRet = PortRet.mean()
AverageReturns = pd.DataFrame(MeanRet)
AverageReturns.columns = ['Average Return']
AverageReturnsPCG = AverageReturns*100

# %% Portfolio Return
PortTable = pd.merge(AverageReturns,SpecificTrader,on='Symbol')
PortTable['Weight'] = PortTable['Notional']/PortTable['Notional'].sum()
PortTable['Weighted Return'] = PortTable['Weight'] * PortTable['Average Return']
PortfolioReturn = PortTable['Weighted Return'].sum()
WeightList = PortTable['Weight'].tolist()
SymbolList = PortTable['Symbol'].tolist()
plt.figure(figsize = (90, 75))
plt.bar(SymbolList,WeightList)
plt.xticks(fontsize=50)
plt.yticks(fontsize=50)
plt.savefig(save_path+'\AssetBreakdown.png')
plt.close()

#%% Variance-Covariance Matrix for stocks
CovMatrix = PortRet.cov()
#%% Portfolio Variance and Standard Deviation
WeightArray = PortTable['Weight'].to_numpy()
PortVariance = CovMatrix.mul(WeightArray,axis=0).mul(WeightArray,axis=1).sum().sum()
PortStdev = np.sqrt(PortVariance)
#%% Correlation Matrix
correlation_matrix = PortRet.corr(method='pearson')
correlation_matrix
#%% Graphics
#Correlation Matrix heatmap
sb.heatmap(correlation_matrix,linewidths=.5,cmap="YlGnBu")
plt.savefig(save_path+'\CorrelationMatrix.png')
plt.close()
#%% Close Price line graph
Closeprice = PortClosePrice.plot(figsize=(90, 75),fontsize=100)
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=70)
plt.xlabel('Date',fontsize=100)
plt.ylabel('Close Price',fontsize=100)
plt.savefig(save_path+'\PortfolioPrices.png')
plt.close()
#%% Volatility Graph for individual stock returns
print('Daily returns for',end_date)
fig, ax = plt.subplots(figsize=(15,8))
for i in PortRet.columns.values :
    ax.plot(PortRet[i], lw =2 ,label = i)
ax.legend( loc='center left', bbox_to_anchor=(1, 0.5), fontsize=10)
ax.set_title('Volatility in daily returns ')
ax.set_xlabel('Date')
ax.set_ylabel('Returns')
plt.savefig(save_path+'\DailyReturns.png')
plt.close()
#%% Risk Box Plot
save_path = os.path.realpath('..')
z = os.path.dirname(__file__)
PortRet.plot(kind = "box",figsize = (20,15))
ax.set_title('Volatility in daily returns ')
plt.xlabel('Stock',fontsize=20)
plt.ylabel('Return',fontsize=20)
plt.title("Risk Box Plot for Stocks in Portfolio",fontsize=20)
plt.savefig(save_path+'\RiskBoxPlot.png')
plt.close()

#%% Ratios Metrics
SPYMeanReturn = NewPricesReturn['SPY'].mean()
PortfolioPremium = PortfolioReturn -SPYMeanReturn
SharpeRatio = (PortfolioReturn-USBondETFReturn['GOVT'].mean())/PortStdev

#%%
html = f'''
    <html>
        <head>
            <title>{TraderID} {end_date}</title>
        </head>
        <body>
            <h1>{TraderID} end of day report for {end_date}</h1>
            <h3>Sector and stock breakdown by weight</h3>
            <h4>{len(StockList)} stocks in the portfolio</h4>
            <img src='SectorBreakdown.png' width="1000">
            <img src='AssetBreakdown.png' width="1000">
            <h2>Portfolio Stock prices for last 5 days</h2>
            {PortClosePrice.tail(5).to_html()}
            <h2>Portfolio Stock Prices from {start_date} to {end_date}</h2>
            <img src='PortfolioPrices.png' width="700">
            <h2>Historical Return Average (%) since {start_date}</h2>
            {round(AverageReturnsPCG,3).to_html()}
            <h2>Risk Box Plot for Stocks in Portfolio</h2>
            <img src='RiskBoxPlot.png' width="1000">
            <h2>Volatility for Stocks in Portfolio</h2>
            <img src='DailyReturns.png' width="1000">
            <h2>Correlation Matrix </h2>
            <img src='CorrelationMatrix.png' width="500">
            <p>The matrix displays the correlation between the stock returns</p>
            <h2>Porftolio Metrics {end_date}</h2>
              <h3>Daily Returns and Ratios</h3>
               <p>According to the historical data, the expected daily portfolio return is {round(PortfolioReturn*100,3)}%</p>
               <p>The SPY ETF daily return is {round(SPYMeanReturn*100,3)}%</p>
               <p>The GOVT ETF return is {round(USBondETFMean*100,3)}%</p>
               <p>The Portfolio out performs the benchmark by {round(PortfolioPremium*100,3)}%</p>
               <p>The Portfolio Sharpe Ratio is {round(SharpeRatio,3)}</p>
              <h3>Porftolio Daily Standard Deviation</h3>
              <p>According to the historical returns, the expected portfolio standard deviation is {round(PortStdev*100,3)}%</p>
              <h3>Porftolio Daily Variance</h3>
              <p>According to the historical returns, the expected portfolio variance is {round(PortVariance*100,3)}%</p>


        </body>
    </html>
    '''
save_path = os.path.realpath('..')
FileName = 'Home.html'
completeName = os.path.join(save_path, 'Home.html')
with open(completeName, 'w') as f:
    f.write(html)

print('Analysis complete for',end_date, 'complete')
# %%
engine.close()
