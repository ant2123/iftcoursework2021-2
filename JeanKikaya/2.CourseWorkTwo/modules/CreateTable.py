# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 22:27:11 2021

@author: Jean Ali Useni Kikaya
"""
# SQL create a new table: trades_suspects
def SQLCreateSuspectTable():
    Command = "CREATE TABLE IF NOT EXISTS trades_suspects (\
    TradeId TEXT PRIMARY KEY,\
    Trader TEXT NOT NULL,\
    Quantity INTEGER,\
    Notional INTEGER,\
    symbol_id TEXT,\
    TradePrice INTEGER,\
    FOREIGN KEY (Trader) REFERENCES equity_prices(symbol_id))"
    return (Command)
