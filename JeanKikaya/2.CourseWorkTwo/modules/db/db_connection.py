# -*- coding: utf-8 -*-
"""
Created on Sat Dec 18 16:41:14 2021

@author: Jean Ali Useni Kikaya
"""
from sqlalchemy import create_engine
import pymongo
from pymongo import MongoClient


#SQL connection
GITRepoDirectory = "C:/Users/aliki/OneDrive/Documents/git"
engine = create_engine(f"sqlite:///{GITRepoDirectory}/iftcoursework2021/000.DataBases/SQL/Equity.db")
con = engine.connect()

#MongoDB connection
cluster = MongoClient("mongodb://127.0.0.1:27017")
db = cluster["Equity"]
collection = db["CourseworkTwo"]
